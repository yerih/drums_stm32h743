/*******************************************************************************
  * @file           : threads.c
  * @brief          : contiene los hilos del app
  * @author			: Yerih Iturriago
  ******************************************************************************/


#include "threads.h"

#ifdef ALGO_

osThreadId_t threadMain_id;//defaultTaskHandle;
const osThreadAttr_t threadMain_attributes = {
  .name = "thread_main",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 128
};




#endif
