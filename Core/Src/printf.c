/*******************************************************************************
  * @file           : printf.c
  * @brief          : control el printf.
  * @author			: Yerih Iturriago
  * @description	: configuracion del printf via serial.
  ******************************************************************************/
#include "printf.h"


extern UART_HandleTypeDef huart4;
//UART_HandleTypeDef printf_huart = huart4;
#define PRINTF_UART huart4


#ifdef __GNUC__
  /* With GCC, small printf (option LD Linker->Libraries->Small printf
     set to 'Yes') calls __io_putchar() */
  #define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
  #define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif /* __GNUC__ */
PUTCHAR_PROTOTYPE
{
  /* Place your implementation of fputc here */
  /* e.g. write a character to the EVAL_COM1 and Loop until the end of transmission */
  HAL_UART_Transmit(&PRINTF_UART, (uint8_t *)&ch, 1, 1);

  return ch;
}



