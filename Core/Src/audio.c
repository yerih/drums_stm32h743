/*******************************************************************************
  * @file           : audio.c
  * @brief          : manejo de audio
  * @author			: Yerih Iturriago
  * @note			:
  *
  ******************************************************************************/

#include "audio.h"


st_AUDIO_BUFFER audioBuffer;


/**
  * @brief  iniciacializa el motor de audio
  * @param  None
  * @retval None
  */
void audio_init()
{
	memset(&audioBuffer, 0, sizeof(st_AUDIO_BUFFER));
	sai_DMA_init(&audioBuffer);
}


/**
  * @brief  reproduce un archivo wav previamente abierto.
  * @param  file:  archivo abierto.
  * @param	power: multiplicador de valor. Se usa para amplitud de onda.
  * @retval None
  */
void audio_playWav(FIL* file, float power)
{
	if(file == NULL)return;
	uint32_t lseek 	   = WAV_HEADER_SIZE;
	uint32_t fsizeData = f_size(file) - WAV_HEADER_SIZE;
	enum_AUDIO_BUF_OPERATION state = WAITING;
	uint8_t flag_read  = FALSE;
	int res;

	DMA_I2S_FORMATBIT buffer[DMA_I2S_BUFFER_SIZE];
	printf("audio_playWav: init\n");

	memset(buffer, 0, sizeof(buffer));
	res = audio_readFile(file, buffer, &lseek);
	if(res != 0)
	{
		if(res < 0)
		{
//			printf("audio_playWav: error in audio_readFile\n");
			g_showDebug ? printf("audio_playWav: error in audio_readFile\n"):0;
			return;
		}
		g_showDebug ? printf("audio_playWav: finish audio read, line %d \n", __LINE__):0;
		return;
	}

	while(lseek < fsizeData)
	{
		if(flag_read)
		{
			flag_read = FALSE;
			res = audio_readFile(file, buffer, &lseek);
			if(res != 0)
			{
				if(res < 0)
				{
					printf("audio_playWav: error in audio_readFile\n");
					g_showDebug ? printf("audio_playWav: error in audio_readFile, flag_read = true, line %d\n", __LINE__):0;
					break;
				}
				printf("audio_playWav: finish audio read, line %d\n", __LINE__);
				g_showDebug ? printf("audio_playWav: error in audio_readFile, flag_read = true, line %d\n", __LINE__):0;
				break;
			}
//			printf("audio_playWav: file read\n");
		}

		if(audioBuffer.memAvailable == MEMORY0 && state != PUT_IN_M0)
		{
			state 	  = PUT_IN_M0;
			flag_read = TRUE;
			mix(audioBuffer.M0, buffer, DMA_I2S_BUFFER_SIZE, power);
//			memcpy(audioBuffer.M0, buffer, sizeof(DMA_I2S_BUFFER_SIZE));
//			printf("audio_playWav: put in M0\n");
		}

		if(audioBuffer.memAvailable == MEMORY1 && state != PUT_IN_M1)
		{
			state 	  = PUT_IN_M1;
			flag_read = TRUE;
			mix(audioBuffer.M1, buffer, DMA_I2S_BUFFER_SIZE, power);
//			memcpy(audioBuffer.M1, buffer, sizeof(DMA_I2S_BUFFER_SIZE));
//			printf("audio_playWav: put in M1\n");
		}
//		printf("audio_playWav: while: lseek = %lu, fsize = %lu\r", lseek, fsizeData);

//		if(
//			((lseek > fsizeData/4)&&(lseek <= fsizeData/2)) ||
//			((lseek > fsizeData/2)&&(lseek <= fsizeData*3/2))
//			((lseek > fsizeData*3/2)&&(lseek <= fsizeData))
//			)
//		{
//			printf("lseek = %lu, fsizeData = %lu\n", lseek, fsizeData);
//		}

	}

	printf("audio_playWav: finish: lseek = %lu, fsize = %lu\r", lseek, fsizeData);
}


/**
  * @brief  lee un archivo wav de la SD previamente abierto.
  * @param  file:  archivo abierto.
  * @retval 1: file finish read, -1: error, 0: success.
  */
int audio_readFile(FIL* file, void* buffer, uint32_t* lseek)
{
	FRESULT res;
	uint32_t length;
	uint32_t fsizeData = f_size(file) - WAV_HEADER_SIZE;
	length = audio_verifyTopFile(fsizeData, *lseek);

	if (length <= 0)
	{
		*lseek = WAV_HEADER_SIZE;
//		length = DMA_I2S_READ_FILE_SIZE;
		return 1;
	}

	res = SD_readFile(file, buffer, length, lseek);
	if (res)
	{
		if(g_showDebug)
		{
			char temp[25];
			printf("audio_playWav: error en SD_readFile: %s\n", utils_fresult(res, temp));
		}
		return -1;
	}

	g_showDebug ? printf("audio_playWav: it's ok. lseek = %lu\n", *lseek) : 0;
	return 0;
}

/***********************************************************************
 input	:	pad : apuntador del tipo de estructura PAD.
 output	:	DMA_I2S_BUFFER_SIZE: si aun no llega al tope del archivo.
 < DMA_I2S_BUFFER_SIZE: resto del archivo.
 descr	:	lee la SD sin abrir ni hacer montaje ni desmontaje.
 Guarda la estructura FIL (SDFile).
 ************************************************************************/
int32_t audio_verifyTopFile(uint32_t fsizeData, uint32_t lseek)
{
	int32_t r = fsizeData - lseek; //resto del archivo

	if (r >= DMA_I2S_READ_FILE_SIZE)
	{
		g_showDebug ? printf("audio_verifyTop: r = %ld, lseek = %lu\n", r, lseek) :0;
		return DMA_I2S_READ_FILE_SIZE;
	}
	else if (r <= 0)
		return 0;
	g_showDebug ? printf("verifyTopFilePad: r = %ld, lseek = %lu\n", r, lseek) :0;
	return r;
}


/**
  * @brief  mezcla en buffer segun la potencia o volumen
  * @param  dest: direccion de destino
  * @param  src:  direccion de fuente
  * @param  len:  tamaño de la transferencia
  * @param  power:  multiplicador de valor. Se usa para volument o potencia
  * @retval 1: file finish read, -1: error, 0: success.
  */
void mix(DMA_I2S_FORMATBIT* dest, DMA_I2S_FORMATBIT* src, uint32_t len, float power)
{
	for(int i = 0; i < len; i++)
		dest[i] += src[i]*power;
}



