/*******************************************************************************
  * @file           : sai_dma_OS.h
  * @brief          : gestion del modulo SAI y su stream DMA asignado
  * @author			: Yerih Iturriago
  ******************************************************************************/


#include "sai_dma_OS.h"

#define SAI_FIFO_SIZE            8U
#define SAI_DEFAULT_TIMEOUT      4U
#define SAI_LONG_TIMEOUT         1000U


static void SAI_DMATxHalfCplt(DMA_HandleTypeDef *hdma);
static void SAI_DMATxCplt(DMA_HandleTypeDef *hdma);
static void SAI_DMAError(DMA_HandleTypeDef *hdma);
static HAL_StatusTypeDef SAI_Disable(SAI_HandleTypeDef *hsai);
static uint32_t SAI_InterruptFlag(const SAI_HandleTypeDef *hsai, SAI_ModeTypedef mode);

static void I2S_DMATxHalfCplt(DMA_HandleTypeDef *hdma);
static void I2S_DMATxCplt(DMA_HandleTypeDef *hdma);
static void I2S_DMAError(DMA_HandleTypeDef *hdma);

static HAL_StatusTypeDef HAL_DMA_Start_IT_OS(DMA_HandleTypeDef *hdma, uint32_t SrcAddress, uint32_t DstAddress, uint32_t DataLength);

#ifdef HAL_DMA_MODULE_ENABLED

/* Private types -------------------------------------------------------------*/
typedef struct
{
  __IO uint32_t ISR;   /*!< DMA interrupt status register */
  __IO uint32_t Reserved0;
  __IO uint32_t IFCR;  /*!< DMA interrupt flag clear register */
} DMA_Base_Registers;

typedef struct
{
  __IO uint32_t ISR;   /*!< BDMA interrupt status register */
  __IO uint32_t IFCR;  /*!< BDMA interrupt flag clear register */
} BDMA_Base_Registers;

#endif
/**
  * @brief  Transmit an amount of data in non-blocking mode with DMA.
  * @param  hsai pointer to a SAI_HandleTypeDef structure that contains
  *              the configuration information for SAI module.
  * @param  pData Pointer to data buffer
  * @param  Size Amount of data to be sent
  * @retval HAL status
  */
HAL_StatusTypeDef HAL_SAI_Transmit_DMA_OS(SAI_HandleTypeDef *hsai, uint8_t *pData, uint16_t Size)
{
  uint32_t tickstart = HAL_GetTick();

  if ((pData == NULL) || (Size == 0U))
  {
    return  HAL_ERROR;
  }

  if (hsai->State == HAL_SAI_STATE_READY)
  {
    /* Process Locked */
    __HAL_LOCK(hsai);

    hsai->pBuffPtr = pData;
    hsai->XferSize = Size;
    hsai->XferCount = Size;
    hsai->ErrorCode = HAL_SAI_ERROR_NONE;
    hsai->State = HAL_SAI_STATE_BUSY_TX;

    /* Set the SAI Tx DMA Half transfer complete callback */
    hsai->hdmatx->XferHalfCpltCallback = SAI_DMATxHalfCplt;

    /* Set the SAI TxDMA transfer complete callback */
    hsai->hdmatx->XferCpltCallback = SAI_DMATxCplt;

    /* Set the DMA error callback */
    hsai->hdmatx->XferErrorCallback = SAI_DMAError;

    /* Set the DMA Tx abort callback */
    hsai->hdmatx->XferAbortCallback = NULL;

    /* USER CODE BEGIN 1 */
    /* Enable the Tx DMA Stream */
    if(HAL_DMAEx_MultiBufferStart_IT(hsai->hdmatx, (uint32_t)hsai->pBuffPtr, (uint32_t)&hsai->Instance->DR, ((DMA_Stream_TypeDef*)hsai->hdmatx->Instance)->M1AR, hsai->XferSize) != HAL_OK)
    {
    	__HAL_UNLOCK(hsai);
    	return  HAL_ERROR;
    }

    /* USER CODE END 1*/

    /* Enable the interrupts for error handling */
    __HAL_SAI_ENABLE_IT(hsai, SAI_InterruptFlag(hsai, SAI_MODE_DMA));

    /* Enable SAI Tx DMA Request */
    hsai->Instance->CR1 |= SAI_xCR1_DMAEN;

    /* Wait untill FIFO is not empty */
    while ((hsai->Instance->SR & SAI_xSR_FLVL) == SAI_FIFOSTATUS_EMPTY)
    {
      /* Check for the Timeout */
      if ((HAL_GetTick() - tickstart) > SAI_LONG_TIMEOUT)
      {
        /* Update error code */
        hsai->ErrorCode |= HAL_SAI_ERROR_TIMEOUT;

        /* Process Unlocked */
        __HAL_UNLOCK(hsai);

        return HAL_TIMEOUT;
      }
    }

    /* Check if the SAI is already enabled */
    if ((hsai->Instance->CR1 & SAI_xCR1_SAIEN) == 0U)
    {
      /* Enable SAI peripheral */
      __HAL_SAI_ENABLE(hsai);
    }

    /* Process Unlocked */
    __HAL_UNLOCK(hsai);

    return HAL_OK;
  }
  else
  {
    return HAL_BUSY;
  }
}


/**
  * @brief  DMA SAI callback half memory complete
  * @param  hsai pointer to a SAI_HandleTypeDef structure that contains
  *         the configuration information for the specified SAI module.
  * @retval None
  */
void HAL_SAI_TxHalfCpltCallback(SAI_HandleTypeDef *hsai)
{
//	uint8_t flag_availMEM = 0;
//	uint32_t CR = ((DMA_Stream_TypeDef*) hsai->Instance)->CR;
//	if ((CR & 0x80000) == 0)
//		flag_availMEM = 1;
//
//	pad_readFilePAD(&pad_snare);
//	memset(!flag_availMEM ? M1 : M0, 0, sizeof(M0));
	uint8_t flag_availMEM = 0;
	uint32_t CR = ((DMA_Stream_TypeDef*) hsai->Instance)->CR;
	if ((CR & 0x80000) == 0)
		flag_availMEM = 1;

	memset(flag_availMEM ? audioBuffer.M1:audioBuffer.M0, 0, sizeof(audioBuffer.M0));
	audioBuffer.memAvailable = flag_availMEM;
//	printf("SAI callback: memAvail = %d\r", flag_availMEM);
}


void HAL_I2S_TxHalfCpltCallback(I2S_HandleTypeDef *hi2s)
{
//	uint8_t flag_availMEM = 0;
//	uint32_t CR = ((DMA_Stream_TypeDef*) hi2s->Instance)->CR;
//	if ((CR & 0x80000) == 0)
//		flag_availMEM = 1;
//
//	pad_readFilePAD(&pad_snare);
//	memcpy(flag_availMEM ? M1 : M0, pad_snare.buffer, DMA_I2S_READ_FILE_SIZE);
//	memset(pad_snare.buffer, 0, sizeof(M0));
	uint8_t flag_availMEM = 0;
	uint32_t CR = ((DMA_Stream_TypeDef*) hi2s->Instance)->CR;
	if ((CR & 0x80000) == 0)
		flag_availMEM = 1;
	memset(flag_availMEM ? audioBuffer.M1:audioBuffer.M0, 0, sizeof(audioBuffer.M0));
	audioBuffer.memAvailable = flag_availMEM;
}



/**
  * @brief  inicializa sai dma en modo circular y double buffer.
  * @param  audioBuffer : contiene las memorias del motor de audio
  * @retval None
  */
void sai_DMA_init(st_AUDIO_BUFFER* audioBuffer)
{
	hsai_BlockA1.hdmatx->XferM1HalfCpltCallback = (void*)HAL_SAI_TxHalfCpltCallback;
	((DMA_Stream_TypeDef   *)hsai_BlockA1.hdmatx->Instance)->M1AR = (uint32_t)audioBuffer->M1;

	if(HAL_OK != HAL_SAI_Transmit_DMA_OS(&hsai_BlockA1, (uint8_t*)audioBuffer->M0, DMA_I2S_BUFFER_SIZE))
	{
		Error_Handler();
	}
}


/**
  * @brief  inicializa i2s dma
  * @param  audioBuffer : contiene las memorias del motor de audio
  * @retval None
  */
void i2s_DMA_init(st_AUDIO_BUFFER* audioBuffer)
{

	hi2s1.hdmatx->XferM1HalfCpltCallback = (void*)HAL_I2S_TxHalfCpltCallback;
	((DMA_Stream_TypeDef   *)hi2s1.hdmatx->Instance)->M1AR = (uint32_t)audioBuffer->M1;

	if(HAL_OK != HAL_I2S_Transmit_DMA_OS(&hi2s1, (uint16_t*)audioBuffer->M0, DMA_I2S_BUFFER_SIZE))
	{
		Error_Handler();
	}

}


/**
  * @brief  DMA SAI transmit process half complete callback.
  * @param  hdma pointer to a DMA_HandleTypeDef structure that contains
  *              the configuration information for the specified DMA module.
  * @retval None
  */
static void SAI_DMATxHalfCplt(DMA_HandleTypeDef *hdma)
{
  SAI_HandleTypeDef *hsai = (SAI_HandleTypeDef *)((DMA_HandleTypeDef *)hdma)->Parent;

#if (USE_HAL_SAI_REGISTER_CALLBACKS == 1)
  hsai->TxHalfCpltCallback(hsai);
#else
  HAL_SAI_TxHalfCpltCallback(hsai);
#endif
}


/**
  * @brief  DMA SAI transmit process complete callback.
  * @param  hdma pointer to a DMA_HandleTypeDef structure that contains
  *              the configuration information for the specified DMA module.
  * @retval None
  */
static void SAI_DMATxCplt(DMA_HandleTypeDef *hdma)
{
  SAI_HandleTypeDef *hsai = (SAI_HandleTypeDef *)((DMA_HandleTypeDef *)hdma)->Parent;

  if (hdma->Init.Mode != DMA_CIRCULAR)
  {
    hsai->XferCount = 0;

    /* Disable SAI Tx DMA Request */
    hsai->Instance->CR1 &= (uint32_t)(~SAI_xCR1_DMAEN);

    /* Stop the interrupts error handling */
    __HAL_SAI_DISABLE_IT(hsai, SAI_InterruptFlag(hsai, SAI_MODE_DMA));

    hsai->State = HAL_SAI_STATE_READY;
  }
#if (USE_HAL_SAI_REGISTER_CALLBACKS == 1)
  hsai->TxCpltCallback(hsai);
#else
  HAL_SAI_TxCpltCallback(hsai);
#endif
}


/**
  * @brief  DMA SAI communication error callback.
  * @param  hdma pointer to a DMA_HandleTypeDef structure that contains
  *              the configuration information for the specified DMA module.
  * @retval None
  */
static void SAI_DMAError(DMA_HandleTypeDef *hdma)
{
  SAI_HandleTypeDef *hsai = (SAI_HandleTypeDef *)((DMA_HandleTypeDef *)hdma)->Parent;

  /* Ignore DMA FIFO error */
  if (HAL_DMA_GetError(hdma) != HAL_DMA_ERROR_FE)
  {
    /* Set SAI error code */
    hsai->ErrorCode |= HAL_SAI_ERROR_DMA;

    /* Disable the SAI DMA request */
    hsai->Instance->CR1 &= ~SAI_xCR1_DMAEN;

    /* Disable SAI peripheral */
    /* No need to check return value because state will be updated and HAL_SAI_ErrorCallback will be called later */
    (void) SAI_Disable(hsai);

    /* Set the SAI state ready to be able to start again the process */
    hsai->State = HAL_SAI_STATE_READY;

    /* Initialize XferCount */
    hsai->XferCount = 0U;

    /* SAI error Callback */
#if (USE_HAL_SAI_REGISTER_CALLBACKS == 1)
    hsai->ErrorCallback(hsai);
#else
    HAL_SAI_ErrorCallback(hsai);
#endif
  }
}


/**
  * @brief  Disable the SAI and wait for the disabling.
  * @param  hsai pointer to a SAI_HandleTypeDef structure that contains
  *              the configuration information for SAI module.
  * @retval None
  */
static HAL_StatusTypeDef SAI_Disable(SAI_HandleTypeDef *hsai)
{
  register uint32_t count = SAI_DEFAULT_TIMEOUT * (SystemCoreClock / 7U / 1000U);
  HAL_StatusTypeDef status = HAL_OK;

  /* Disable the SAI instance */
  __HAL_SAI_DISABLE(hsai);

  do
  {
    /* Check for the Timeout */
    if (count == 0U)
    {
      /* Update error code */
      hsai->ErrorCode |= HAL_SAI_ERROR_TIMEOUT;
      status = HAL_TIMEOUT;
      break;
    }
    count--;
  }
  while ((hsai->Instance->CR1 & SAI_xCR1_SAIEN) != 0U);

  return status;
}


/**
  * @brief  Return the interrupt flag to set according the SAI setup.
  * @param  hsai pointer to a SAI_HandleTypeDef structure that contains
  *              the configuration information for SAI module.
  * @param  mode SAI_MODE_DMA or SAI_MODE_IT
  * @retval the list of the IT flag to enable
  */
static uint32_t SAI_InterruptFlag(const SAI_HandleTypeDef *hsai, SAI_ModeTypedef mode)
{
  uint32_t tmpIT = SAI_IT_OVRUDR;

  if (mode == SAI_MODE_IT)
  {
    tmpIT |= SAI_IT_FREQ;
  }

  if ((hsai->Init.Protocol == SAI_AC97_PROTOCOL) &&
      ((hsai->Init.AudioMode == SAI_MODESLAVE_RX) || (hsai->Init.AudioMode == SAI_MODEMASTER_RX)))
  {
    tmpIT |= SAI_IT_CNRDY;
  }

  if ((hsai->Init.AudioMode == SAI_MODESLAVE_RX) || (hsai->Init.AudioMode == SAI_MODESLAVE_TX))
  {
    tmpIT |= SAI_IT_AFSDET | SAI_IT_LFSDET;
  }
  else
  {
    /* hsai has been configured in master mode */
    tmpIT |= SAI_IT_WCKCFG;
  }
  return tmpIT;
}






/**
  * @brief  Transmit an amount of data in non-blocking mode with DMA
  * @param  hi2s pointer to a I2S_HandleTypeDef structure that contains
  *         the configuration information for I2S module
  * @param  pData a 16-bit pointer to the Transmit data buffer.
  * @param  Size number of data sample to be sent:
  * @note   When a 16-bit data frame or a 16-bit data frame extended is selected during the I2S
  *         configuration phase, the Size parameter means the number of 16-bit data length
  *         in the transaction and when a 24-bit data frame or a 32-bit data frame is selected
  *         the Size parameter means the number of 16-bit data length.
  * @note   The I2S is kept enabled at the end of transaction to avoid the clock de-synchronization
  *         between Master and Slave(example: audio streaming).
  * @retval HAL status
  */
HAL_StatusTypeDef HAL_I2S_Transmit_DMA_OS(I2S_HandleTypeDef *hi2s, uint16_t *pData, uint16_t Size)
{
  if ((pData == NULL) || (Size == 0UL))
  {
    return  HAL_ERROR;
  }

  /* Process Locked */
  __HAL_LOCK(hi2s);

  if (hi2s->State != HAL_I2S_STATE_READY)
  {
    __HAL_UNLOCK(hi2s);
    return HAL_BUSY;
  }

  /* Set state and reset error code */
  hi2s->State       = HAL_I2S_STATE_BUSY_TX;
  hi2s->ErrorCode   = HAL_I2S_ERROR_NONE;
  hi2s->pTxBuffPtr  = pData;
  hi2s->TxXferSize  = Size;
  hi2s->TxXferCount = Size;

  /* Init field not used in handle to zero */
  hi2s->pRxBuffPtr  = NULL;
  hi2s->RxXferSize  = (uint16_t)0UL;
  hi2s->RxXferCount = (uint16_t)0UL;

  /* Set the I2S Tx DMA Half transfer complete callback */
  hi2s->hdmatx->XferHalfCpltCallback = I2S_DMATxHalfCplt;

  /* Set the I2S Tx DMA transfer complete callback */
  hi2s->hdmatx->XferCpltCallback = I2S_DMATxCplt;

  /* Set the DMA error callback */
  hi2s->hdmatx->XferErrorCallback = I2S_DMAError;



  /* Enable the Tx DMA Stream/Channel */
  /* Enable the Tx DMA Stream/Channel */
  if (HAL_OK != HAL_DMA_Start_IT_OS(hi2s->hdmatx, (uint32_t)hi2s->pTxBuffPtr, (uint32_t)&hi2s->Instance->TXDR, hi2s->TxXferSize))
  {
	  /* Update SPI error code */
	  SET_BIT(hi2s->ErrorCode, HAL_I2S_ERROR_DMA);
	  hi2s->State = HAL_I2S_STATE_READY;

	  __HAL_UNLOCK(hi2s);
	  return HAL_ERROR;
  }

  /* Check if the I2S Tx request is already enabled */
  if (HAL_IS_BIT_CLR(hi2s->Instance->CFG1, SPI_CFG1_TXDMAEN))
  {
    /* Enable Tx DMA Request */
    SET_BIT(hi2s->Instance->CFG1, SPI_CFG1_TXDMAEN);
  }

  /* Check if the I2S is already enabled */
  if (HAL_IS_BIT_CLR(hi2s->Instance->CR1, SPI_CR1_SPE))
  {
    /* Enable I2S peripheral */
    __HAL_I2S_ENABLE(hi2s);
  }

  /* Start the transfer */
  SET_BIT(hi2s->Instance->CR1, SPI_CR1_CSTART);

  __HAL_UNLOCK(hi2s);
  return HAL_OK;
}



static HAL_StatusTypeDef HAL_DMA_Start_IT_OS(DMA_HandleTypeDef *hdma, uint32_t SrcAddress, uint32_t DstAddress, uint32_t DataLength)
{
  HAL_StatusTypeDef status = HAL_OK;

  /* Check the parameters */
  assert_param(IS_DMA_BUFFER_SIZE(DataLength));

  /* Check the DMA peripheral handle */
  if(hdma == NULL)
  {
    return HAL_ERROR;
  }

  /* Process locked */
  __HAL_LOCK(hdma);

  if(HAL_DMA_STATE_READY == hdma->State)
  {
    /* Change DMA peripheral state */
    hdma->State = HAL_DMA_STATE_BUSY;

    /* Initialize the error code */
    hdma->ErrorCode = HAL_DMA_ERROR_NONE;

    /* Disable the peripheral */
    __HAL_DMA_DISABLE(hdma);

    /* Configure the source, destination address and the data length */
//    DMA_SetConfig(hdma, SrcAddress, DstAddress, DataLength);
    {
    	/* calculate DMA base and stream number */
    	  DMA_Base_Registers  *regs_dma  = (DMA_Base_Registers *)hdma->StreamBaseAddress;
    	  BDMA_Base_Registers *regs_bdma = (BDMA_Base_Registers *)hdma->StreamBaseAddress;

    	  if(IS_DMA_DMAMUX_ALL_INSTANCE(hdma->Instance) != 0U) /* No DMAMUX available for BDMA1 */
    	  {
    	    /* Clear the DMAMUX synchro overrun flag */
    	    hdma->DMAmuxChannelStatus->CFR = hdma->DMAmuxChannelStatusMask;

    	    if(hdma->DMAmuxRequestGen != 0U)
    	    {
    	      /* Clear the DMAMUX request generator overrun flag */
    	      hdma->DMAmuxRequestGenStatus->RGCFR = hdma->DMAmuxRequestGenStatusMask;
    	    }
    	  }

    	  if(IS_DMA_STREAM_INSTANCE(hdma->Instance) != 0U) /* DMA1 or DMA2 instance */
    	  {
    	    /* Clear all interrupt flags at correct offset within the register */
    	    regs_dma->IFCR = 0x3FUL << (hdma->StreamIndex & 0x1FU);

    	    /* Enable the Double buffer mode */
    	    ((DMA_Stream_TypeDef   *)hdma->Instance)->CR |= DMA_SxCR_DBM;

    	    /* Configure DMA Stream destination address */
//    	    ((DMA_Stream_TypeDef   *)hdma->Instance)->M1AR = SecondMemAddress;

    	    /* Configure DMA Stream data length */
    	    ((DMA_Stream_TypeDef *)hdma->Instance)->NDTR = DataLength;

    	    /* Peripheral to Memory */
    	    if((hdma->Init.Direction) == DMA_MEMORY_TO_PERIPH)
    	    {
    	      /* Configure DMA Stream destination address */
    	      ((DMA_Stream_TypeDef *)hdma->Instance)->PAR = DstAddress;

    	      /* Configure DMA Stream source address */
    	      ((DMA_Stream_TypeDef *)hdma->Instance)->M0AR = SrcAddress;
    	    }
    	    /* Memory to Peripheral */
    	    else
    	    {
    	      /* Configure DMA Stream source address */
    	      ((DMA_Stream_TypeDef *)hdma->Instance)->PAR = SrcAddress;

    	      /* Configure DMA Stream destination address */
    	      ((DMA_Stream_TypeDef *)hdma->Instance)->M0AR = DstAddress;
    	    }
    	  }
    	  else if(IS_BDMA_CHANNEL_INSTANCE(hdma->Instance) != 0U) /* BDMA instance(s) */
    	  {
    	    /* Clear all flags */
    	    regs_bdma->IFCR = (BDMA_ISR_GIF0) << (hdma->StreamIndex & 0x1FU);

    	    /* Configure DMA Channel data length */
    	    ((BDMA_Channel_TypeDef *)hdma->Instance)->CNDTR = DataLength;

    	    /* Peripheral to Memory */
    	    if((hdma->Init.Direction) == DMA_MEMORY_TO_PERIPH)
    	    {
    	      /* Configure DMA Channel destination address */
    	      ((BDMA_Channel_TypeDef *)hdma->Instance)->CPAR = DstAddress;

    	      /* Configure DMA Channel source address */
    	      ((BDMA_Channel_TypeDef *)hdma->Instance)->CM0AR = SrcAddress;
    	    }
    	    /* Memory to Peripheral */
    	    else
    	    {
    	      /* Configure DMA Channel source address */
    	      ((BDMA_Channel_TypeDef *)hdma->Instance)->CPAR = SrcAddress;

    	      /* Configure DMA Channel destination address */
    	      ((BDMA_Channel_TypeDef *)hdma->Instance)->CM0AR = DstAddress;
    	    }
    	  }
    	  else
    	  {
    	    /* Nothing To Do */
    	  }
    }



    if(IS_DMA_STREAM_INSTANCE(hdma->Instance) != 0U) /* DMA1 or DMA2 instance */
    {
      /* Enable Common interrupts*/
      MODIFY_REG(((DMA_Stream_TypeDef   *)hdma->Instance)->CR, (DMA_IT_TC | DMA_IT_TE | DMA_IT_DME | DMA_IT_HT), (DMA_IT_TC | DMA_IT_TE | DMA_IT_DME));

      if(hdma->XferHalfCpltCallback != NULL)
      {
        /* Enable Half Transfer IT if corresponding Callback is set */
        ((DMA_Stream_TypeDef   *)hdma->Instance)->CR  |= DMA_IT_HT;
      }
    }
    else /* BDMA channel */
    {
      /* Enable Common interrupts */
      MODIFY_REG(((BDMA_Channel_TypeDef   *)hdma->Instance)->CCR, (BDMA_CCR_TCIE | BDMA_CCR_HTIE | BDMA_CCR_TEIE), (BDMA_CCR_TCIE | BDMA_CCR_TEIE));

      if(hdma->XferHalfCpltCallback != NULL)
      {
        /*Enable Half Transfer IT if corresponding Callback is set */
        ((BDMA_Channel_TypeDef   *)hdma->Instance)->CCR  |= BDMA_CCR_HTIE;
      }
    }

    if(IS_DMA_DMAMUX_ALL_INSTANCE(hdma->Instance) != 0U) /* No DMAMUX available for BDMA1 */
    {
      /* Check if DMAMUX Synchronization is enabled */
      if((hdma->DMAmuxChannel->CCR & DMAMUX_CxCR_SE) != 0U)
      {
        /* Enable DMAMUX sync overrun IT*/
        hdma->DMAmuxChannel->CCR |= DMAMUX_CxCR_SOIE;
      }

      if(hdma->DMAmuxRequestGen != 0U)
      {
        /* if using DMAMUX request generator, enable the DMAMUX request generator overrun IT*/
        /* enable the request gen overrun IT */
        hdma->DMAmuxRequestGen->RGCR |= DMAMUX_RGxCR_OIE;
      }
    }

    /* Enable the Peripheral */
    __HAL_DMA_ENABLE(hdma);
  }
  else
  {
    /* Process unlocked */
    __HAL_UNLOCK(hdma);

    /* Set the error code to busy */
    hdma->ErrorCode = HAL_DMA_ERROR_BUSY;

    /* Return error status */
    status = HAL_ERROR;
  }

  return status;
}





/**
  * @brief  DMA I2S transmit process half complete callback
  * @param  hdma pointer to a DMA_HandleTypeDef structure that contains
  *         the configuration information for the specified DMA module.
  * @retval None
  */
static void I2S_DMATxHalfCplt(DMA_HandleTypeDef *hdma)
{
  I2S_HandleTypeDef *hi2s = (I2S_HandleTypeDef *)((DMA_HandleTypeDef *)hdma)->Parent; /* Derogation MISRAC2012-Rule-11.5 */

  /* Call user Tx half complete callback */
#if (USE_HAL_I2S_REGISTER_CALLBACKS == 1UL)
  hi2s->TxHalfCpltCallback(hi2s);
#else
  HAL_I2S_TxHalfCpltCallback(hi2s);
#endif /* USE_HAL_I2S_REGISTER_CALLBACKS */
}





/**
  * @brief  DMA I2S transmit process complete callback
  * @param  hdma pointer to a DMA_HandleTypeDef structure that contains
  *                the configuration information for the specified DMA module.
  * @retval None
  */
static void I2S_DMATxCplt(DMA_HandleTypeDef *hdma)
{
  I2S_HandleTypeDef *hi2s = (I2S_HandleTypeDef *)((DMA_HandleTypeDef *)hdma)->Parent; /* Derogation MISRAC2012-Rule-11.5 */

  /* if DMA is configured in DMA_NORMAL Mode */
  if (hdma->Init.Mode == DMA_NORMAL)
  {
    /* Disable Tx DMA Request */
    CLEAR_BIT(hi2s->Instance->CFG1, SPI_CFG1_TXDMAEN);

    hi2s->TxXferCount = (uint16_t) 0UL;
    hi2s->State = HAL_I2S_STATE_READY;
  }
  /* Call user Tx complete callback */
#if (USE_HAL_I2S_REGISTER_CALLBACKS == 1UL)
  hi2s->TxCpltCallback(hi2s);
#else
  HAL_I2S_TxCpltCallback(hi2s);
#endif /* USE_HAL_I2S_REGISTER_CALLBACKS */
}




/**
  * @brief  DMA I2S communication error callback
  * @param  hdma pointer to a DMA_HandleTypeDef structure that contains
  *         the configuration information for the specified DMA module.
  * @retval None
  */
static void I2S_DMAError(DMA_HandleTypeDef *hdma)
{
  I2S_HandleTypeDef *hi2s = (I2S_HandleTypeDef *)((DMA_HandleTypeDef *)hdma)->Parent; /* Derogation MISRAC2012-Rule-11.5 */

  /* Disable Rx and Tx DMA Request */
  CLEAR_BIT(hi2s->Instance->CFG1, (SPI_CFG1_RXDMAEN | SPI_CFG1_TXDMAEN));
  hi2s->TxXferCount = (uint16_t) 0UL;
  hi2s->RxXferCount = (uint16_t) 0UL;

  hi2s->State = HAL_I2S_STATE_READY;

  /* Set the error code and execute error callback*/
  SET_BIT(hi2s->ErrorCode, HAL_I2S_ERROR_DMA);
  /* Call user error callback */
#if (USE_HAL_I2S_REGISTER_CALLBACKS == 1UL)
  hi2s->ErrorCallback(hi2s);
#else
  HAL_I2S_ErrorCallback(hi2s);
#endif /* USE_HAL_I2S_REGISTER_CALLBACKS */
}
