/*******************************************************************************
  * @file           : unitTest_stm32h743.c
  * @brief          : utilidades
  * @author			: Yerih Iturriago
  * @note			:
  *
  ******************************************************************************/


#include "unitTest_stm32h743.h"


/**********************************************************************
function: 	unitTest_SD_testing
input	:	mode: WRITE: crea el archivo y lo edita.
				  READ : lee el archivo.

output	:	si hubo algun error retorna 1. De lo contrario 0.
descr	:	realiza pruebas con la SD y muestra resultados con printf.
			Realiza: open, write, read, close.
***********************************************************************/
void unitTest_SD_testing(char* fileName)
{
	  g_showDebug = 1;
	  int i = 0;
	  g_showDebug = 1;
	  i = SD_mount();
	  printf("sd_mount = %d\n", i);

	  i = SD_openFile(fileName, FA_WRITE|FA_READ);
	  printf("sd_open = %d\n", i);

	  i = SD_writeFile(fileName, "Hola soy la SD y escribi aqui", strlen("Hola soy la SD y escribi aqui"));
	  printf("sd_write = %d\n", i);

	  char buffer[50];
	  uint32_t lseek = 0;
	  memset(buffer, 0, sizeof(buffer));
	  i = SD_readFile(&SDFile, &buffer, 50*sizeof(char), &lseek);
	  printf("sd_read = %d\n", i);
	  printf("buffer = %s\n", buffer);

	  i = SD_closeFile(&SDFile);
	  printf("sd_close = %d\n", i);

	  i = SD_unmount();
	  printf("sd_unmount = %d\n", i);
}


/**********************************************************************
input	:	pad:
output	:	noth
descr	:	realiza pruebas de lectura de un pad.
***********************************************************************/
void unitTest_readFilePAD(st_PAD* pad)
{
	pad_initPADdefault(pad);
	g_showDebug = 1;
	while(pad->lseek < pad->iFile[pad->indexFile].file.fileHeaderInfo.FileSize)
		pad_readFilePAD(pad);
	printf("unitTest_readFilePAD: fileSize = %lu, lseek = %lu\n", pad->iFile[pad->indexFile].file.fileHeaderInfo.FileSize, pad->lseek);
}


/**********************************************************************
input	:	pad:
output	:	noth
descr	:	lee el archivo sine.wav y lo coloca en el pad
***********************************************************************/
uint8_t unitTest_readSineFilePAD(st_PAD* pad)
{
	char* fileName = "sine.wav";
	FIL* SDfile    = &pad->iFile[0].SD.file;
	FATFS* SDfatFs = &pad->iFile[0].SD.fatFS;
	g_showDebug = 1;
	//open file
	if(SD_openFile(fileName, FA_READ) != FR_OK)return 1;
	*SDfile  = SDFile;
	*SDfatFs = SDFatFS;
	if(SD_readFile(SDfile, pad->buffer, DMA_I2S_BUFFER_SIZE, &pad->lseek) != FR_OK)
	{
		g_showDebug = 0;
		return 1;
	}
	g_showDebug = 0;
	return 0;
}



/**********************************************************************
input	:	dma stream
output	:	noth
descr	:	lee el archivo sine.wav y lo coloca en el pad
***********************************************************************/
uint8_t unitTest_readDMAregisterCR(void* dmaStream)
{
	uint32_t CR = ((DMA_Stream_TypeDef*)dmaStream)->CR;
	printf("DBM = %lu" , CR &= DMA_SxCR_DBM);
	return 0;
}
