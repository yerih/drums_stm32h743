/*******************************************************************************
 * @file           : pad.c
 * @brief          : manejo de audio
 * @author			: Yerih Iturriago
 * @note			:
 ******************************************************************************/

#include "pad.h"

/*------------------------------------------------------------------- Definiciones ------------------------------------*/
/* Comentar si se desean todos los pads */
#define ONLY_SNARE //recordar quitar definiciones de call.c y utils.c
//#define WITH_KICK
//#define WITH_HITHAT
#if defined(WITH_HITHAT) && !defined(WITH_KICK)
#define WITH_KICK
#endif
#define WITHOUT_PREV

/*--------------------------------------------------------------------- Globales --------------------------------------*/

//------------------------------------------- Buffers

DMA_I2S_FORMATBIT padBuffer_SNARE[1];
#ifndef WITHOUT_PREV
DMA_I2S_FORMATBIT padBuffer_SNAREprev[DMA_I2S_BUFFER_SIZE];
#endif

#if !(defined(ONLY_SNARE) && !defined(WITH_KICK))
DMA_I2S_FORMATBIT padBuffer_KICK[DMA_I2S_BUFFER_SIZE];
#ifndef WITHOUT_PREV
DMA_I2S_FORMATBIT padBuffer_KICKprev[DMA_I2S_BUFFER_SIZE];
#endif
#endif
#if !(defined(ONLY_SNARE) && !defined(WITH_HITHAT))
DMA_I2S_FORMATBIT padBuffer_HITHAT[DMA_I2S_BUFFER_SIZE];
#ifndef WITHOUT_PREV
DMA_I2S_FORMATBIT padBuffer_HITHATprev[DMA_I2S_BUFFER_SIZE];
#endif
#endif
#ifndef ONLY_SNARE
DMA_I2S_FORMATBIT padBuffer_TOM1[DMA_I2S_BUFFER_SIZE];
#ifndef WITHOUT_PREV
DMA_I2S_FORMATBIT padBuffer_TOM1prev[DMA_I2S_BUFFER_SIZE];
#endif
DMA_I2S_FORMATBIT padBuffer_TOM2[DMA_I2S_BUFFER_SIZE];
#ifndef WITHOUT_PREV
DMA_I2S_FORMATBIT padBuffer_TOM2prev[DMA_I2S_BUFFER_SIZE];
#endif
DMA_I2S_FORMATBIT padBuffer_TOM3[DMA_I2S_BUFFER_SIZE];
#ifndef WITHOUT_PREV
DMA_I2S_FORMATBIT padBuffer_TOM3prev[DMA_I2S_BUFFER_SIZE];
#endif
DMA_I2S_FORMATBIT padBuffer_CYMBAL_L[DMA_I2S_BUFFER_SIZE];
#ifndef WITHOUT_PREV
DMA_I2S_FORMATBIT padBuffer_CYMBAL_Lprev[DMA_I2S_BUFFER_SIZE];
#endif
DMA_I2S_FORMATBIT padBuffer_CYMBAL_R[DMA_I2S_BUFFER_SIZE];
#ifndef WITHOUT_PREV
DMA_I2S_FORMATBIT padBuffer_CYMBAL_Rprev[DMA_I2S_BUFFER_SIZE];
#endif
#endif

//----------------------------------------------- PADs
st_PAD pad_snare;

#if !defined(ONLY_SNARE) || defined(WITH_KICK)
st_PAD pad_kick;
#endif
#if !defined(ONLY_SNARE) || defined(WITH_HTIHAT)
st_PAD pad_hithat;
#endif
#ifndef ONLY_SNARE
st_PAD pad_tom1;
st_PAD pad_tom2;
st_PAD pad_tom3;
st_PAD pad_cymbal_L;
st_PAD pad_cymbal_R;
#endif

//---------------------------------------------------------------------

/******************************************************************
 input	:	pad: estructura a inicializar
 output	:	nothing
 descr	:	inicializa un pad.
 *******************************************************************/
void pad_initPAD(st_PAD *pad)
{
	memset(pad, 0, sizeof(st_PAD));
	pad_setBufferPAD(pad);
	pad_setIdPAD(pad);
	pad_setDirPADDefault(pad);
	pad_setFilePADDefault(pad);
	pad_setPathPADDefault(pad);
	pad_openFilesPAD(pad);
	pad_readHeaderFilesPAD(pad);
}

/******************************************************************
 input	:	pad: estructura a inicializar
 output	:	nothing
 descr	:	inicializa un pad.
 *******************************************************************/
void pad_initPADdefault(st_PAD *pad)
{
	memset(pad, 0, sizeof(st_PAD));
//	pad_setBufferPAD(pad);
	pad_setIdPAD(pad);
	pad_setDirPADDefault(pad);
	pad_setFilePADDefault(pad);
	pad_setPathPADDefault(pad);
	if (pad_openFilesPAD(pad))
	{
		printf("pad_openFilesPAD: falla\n");
		return;
	}
	if (pad_readHeaderFilesPAD(pad))
	{
		printf("pad_readHeaderFilesPAD: falla\n");
		return;
	}
}

/******************************************************************
 input	:	pad: estructura a inicializar
 output	:	1: error. 0: exito
 descr	:	setea el Id.
 *******************************************************************/
uint8_t pad_setIdPAD(st_PAD *pad)
{
	if (pad == NULL)
		return 1;
	if (pad == &pad_snare)
		pad->id = PAD_SNARE;
#if defined(WITH_KICK) || !defined(ONLY_SNARE)
	else if (pad == &pad_kick)
		pad->id = PAD_KICK;
#endif
#if defined(WITH_HITHAT) || !defined(ONLY_SNARE)
	else if (pad == &pad_hithat)
		pad->id = PAD_HITHAT;
#endif
#ifndef ONLY_SNARE
	else if (pad == &pad_tom1)
		pad->id = PAD_TOM1;
	else if (pad == &pad_tom2)
		pad->id = PAD_TOM2;
	else if (pad == &pad_tom3)
		pad->id = PAD_TOM3;
	else if (pad == &pad_cymbal_L)
		pad->id = PAD_CYMBAL_L;
	else if (pad == &pad_cymbal_R)
		pad->id = PAD_CYMBAL_R;
#endif
	else
		pad->id = NO_PAD;
	return 0;
}

/******************************************************************
 input	:	pad: estructura a inicializar
 output	:	1: error. 0: exito
 descr	:	setea el buffer del pad.
 *******************************************************************/
uint8_t pad_setBufferPAD(st_PAD *pad)
{
	if (pad == NULL)
		return 1;
	if (pad == &pad_snare)
		pad->buffer = padBuffer_SNARE;
#if defined(WITH_KICK) || !defined(ONLY_SNARE)
	else if (pad == &pad_kick)
		pad->buffer = padBuffer_KICK;
#endif
#if defined(WITH_HITHAT) || !defined(ONLY_SNARE)
	else if (pad == &pad_hithat)
		pad->buffer = padBuffer_HITHAT;
#endif
#ifndef ONLY_SNARE
	else if (pad == &pad_tom1)
		pad->buffer = padBuffer_TOM1;
	else if (pad == &pad_tom2)
		pad->buffer = padBuffer_TOM2;
	else if (pad == &pad_tom3)
		pad->buffer = padBuffer_TOM3;
	else if (pad == &pad_cymbal_L)
		pad->buffer = padBuffer_CYMBAL_L;
	else if (pad == &pad_cymbal_R)
		pad->buffer = padBuffer_CYMBAL_R;
#endif
	else
		pad->buffer = NULL;
	return 0;
}

/******************************************************************
 input	:	pad: estructura a inicializar
 output	:	1: error. 0: exito
 descr	:	setea el Id.
 *******************************************************************/
DMA_I2S_FORMATBIT* pad_getBufferPAD(st_PAD *pad)
{
	return pad->buffer;
}
/******************************************************************
 input	:	pad: estructura a inicializar
 output	:	1: error. 0: exito.
 descr	:	setea el directorio del pad
 *******************************************************************/
uint8_t pad_setDirPADDefault(st_PAD *pad)
{
	if (pad == NULL)
		return 1;
	if (pad == &pad_snare)
		sprintf(pad->pathDir, "/%s/%s/%s", PAD_PATH_AUDIO_LIB, PAD_PATH_DEFAULT,
		PAD_PATH_SNARE);
#if defined(WITH_KICK) || !defined(ONLY_SNARE)
	else if (pad == &pad_kick)
		sprintf(pad->pathDir, "/%s/%s/%s", PAD_PATH_AUDIO_LIB, PAD_PATH_DEFAULT,
		PAD_PATH_KICK);
#endif
#if defined(WITH_HITHAT) || !defined(ONLY_SNARE)
	else if (pad == &pad_hithat)
		sprintf(pad->pathDir, "/%s/%s/%s", PAD_PATH_AUDIO_LIB, PAD_PATH_DEFAULT,
		PAD_PATH_HITHAT);
#endif
#ifndef ONLY_SNARE
	else if (pad == &pad_tom1)
		sprintf(pad->pathDir, "/%s/%s/%s", PAD_PATH_AUDIO_LIB, PAD_PATH_DEFAULT,
		PAD_PATH_TOM1);
	else if (pad == &pad_tom2)
		sprintf(pad->pathDir, "/%s/%s/%s", PAD_PATH_AUDIO_LIB, PAD_PATH_DEFAULT,
		PAD_PATH_TOM2);
	else if (pad == &pad_tom3)
		sprintf(pad->pathDir, "/%s/%s/%s", PAD_PATH_AUDIO_LIB, PAD_PATH_DEFAULT,
		PAD_PATH_TOM3);
	else if (pad == &pad_cymbal_L)
		sprintf(pad->pathDir, "/%s/%s/%s", PAD_PATH_AUDIO_LIB, PAD_PATH_DEFAULT,
		PAD_PATH_CYMBAL_L);
	else if (pad == &pad_cymbal_R)
		sprintf(pad->pathDir, "/%s/%s/%s", PAD_PATH_AUDIO_LIB, PAD_PATH_DEFAULT,
		PAD_PATH_CYMBAL_R);
#endif
	else
		sprintf(pad->pathDir, "/%s/%s/%s", PAD_PATH_AUDIO_LIB, PAD_PATH_DEFAULT,
		PAD_PATH_SNARE);
	return 0;
}

/******************************************************************
 input	:	pad: estructura a inicializar
 output	:	1: error. 0: exito.
 descr	:	setea el directorio del pad
 *******************************************************************/
uint8_t pad_setDirPAD(st_PAD *pad, char *pathDir)
{
	if (pad == NULL)
		return 1;
	if (pad == &pad_snare)
		sprintf(pad->pathDir, "/%s/%s/%s", PAD_PATH_AUDIO_LIB, pathDir,
				PAD_PATH_SNARE);
#if defined(WITH_KICK) || !defined(ONLY_SNARE)
	else if (pad == &pad_kick)
		sprintf(pad->pathDir, "/%s/%s/%s", PAD_PATH_AUDIO_LIB, pathDir, PAD_PATH_KICK);
#endif
#if defined(WITH_HITHAT) || !defined(ONLY_SNARE)
	else if (pad == &pad_hithat)
		sprintf(pad->pathDir, "/%s/%s/%s", PAD_PATH_AUDIO_LIB, pathDir, PAD_PATH_HITHAT);
#endif
#ifndef ONLY_SNARE
	else if (pad == &pad_tom1)
		sprintf(pad->pathDir, "/%s/%s/%s", PAD_PATH_AUDIO_LIB, pathDir, PAD_PATH_TOM1);
	else if (pad == &pad_tom2)
		sprintf(pad->pathDir, "/%s/%s/%s", PAD_PATH_AUDIO_LIB, pathDir,
		PAD_PATH_TOM2);
	else if (pad == &pad_tom3)
		sprintf(pad->pathDir, "/%s/%s/%s", PAD_PATH_AUDIO_LIB, pathDir,
		PAD_PATH_TOM3);
	else if (pad == &pad_cymbal_L)
		sprintf(pad->pathDir, "/%s/%s/%s", PAD_PATH_AUDIO_LIB, pathDir,
		PAD_PATH_CYMBAL_L);
	else if (pad == &pad_cymbal_R)
		sprintf(pad->pathDir, "/%s/%s/%s", PAD_PATH_AUDIO_LIB, pathDir,
		PAD_PATH_CYMBAL_R);
#endif
	else
		sprintf(pad->pathDir, "/%s/%s/%s", PAD_PATH_AUDIO_LIB, pathDir,
		PAD_PATH_SNARE);
	return 0;
}

/******************************************************************
 input	:	pad: estructura a inicializar
 output	:	1: error. 0: exito.
 descr	:	setea los archivos del pad
 *******************************************************************/
uint8_t pad_setFilePADDefault(st_PAD *pad)
{
	if (pad == NULL)
		return 1;
	if (pad == &pad_snare)
	{
		sprintf(pad->iFile[0].fileName, "snare_031.wav");
		sprintf(pad->iFile[1].fileName, "snare_062.wav");
		sprintf(pad->iFile[2].fileName, "snare_093.wav");
		sprintf(pad->iFile[3].fileName, "snare_124.wav");
	}
#if defined(WITH_KICK) || !defined(ONLY_SNARE)
	else if (pad == &pad_kick)
	{
		sprintf(pad->iFile[0].fileName, "kick_031.wav");
		sprintf(pad->iFile[1].fileName, "kick_062.wav");
		sprintf(pad->iFile[2].fileName, "kick_093.wav");
		sprintf(pad->iFile[3].fileName, "kick_124.wav");
	}
#endif
#if defined(WITH_HITHAT) || !defined(ONLY_SNARE)
	else if (pad == &pad_hithat)
	{
		sprintf(pad->iFile[0].fileName, "hithat_031.wav");
		sprintf(pad->iFile[1].fileName, "hithat_062.wav");
		sprintf(pad->iFile[2].fileName, "hithat_093.wav");
		sprintf(pad->iFile[3].fileName, "hithat_124.wav");
	}
#endif
#ifndef ONLY_SNARE
	else if (pad == &pad_tom1)
	{
		sprintf(pad->iFile[0].fileName, "tom1_031.wav");
		sprintf(pad->iFile[1].fileName, "tom1_062.wav");
		sprintf(pad->iFile[2].fileName, "tom1_093.wav");
		sprintf(pad->iFile[3].fileName, "tom1_124.wav");
	}
	else if (pad == &pad_tom2)
	{
		sprintf(pad->iFile[0].fileName, "tom2_031.wav");
		sprintf(pad->iFile[1].fileName, "tom2_062.wav");
		sprintf(pad->iFile[2].fileName, "tom2_093.wav");
		sprintf(pad->iFile[3].fileName, "tom2_124.wav");
	}
	else if (pad == &pad_tom3)
	{
		sprintf(pad->iFile[0].fileName, "tom3_031.wav");
		sprintf(pad->iFile[1].fileName, "tom3_062.wav");
		sprintf(pad->iFile[2].fileName, "tom3_093.wav");
		sprintf(pad->iFile[3].fileName, "tom3_124.wav");
	}
	else if (pad == &pad_cymbal_L)
	{
		sprintf(pad->iFile[0].fileName, "cymbal_L_031.wav");
		sprintf(pad->iFile[1].fileName, "cymbal_L_062.wav");
		sprintf(pad->iFile[2].fileName, "cymbal_L_093.wav");
		sprintf(pad->iFile[3].fileName, "cymbal_L_124.wav");
	}
	else if (pad == &pad_cymbal_R)
	{
		sprintf(pad->iFile[0].fileName, "cymbal_R_031.wav");
		sprintf(pad->iFile[1].fileName, "cymbal_R_062.wav");
		sprintf(pad->iFile[2].fileName, "cymbal_R_093.wav");
		sprintf(pad->iFile[3].fileName, "cymbal_R_124.wav");
	}
#endif
	else
	{
		sprintf(pad->iFile[0].fileName, "no_file.wav");
		sprintf(pad->iFile[1].fileName, "no_file.wav");
		sprintf(pad->iFile[2].fileName, "no_file.wav");
		sprintf(pad->iFile[3].fileName, "no_file.wav");
	}
	return 0;
}

/******************************************************************
 input	:	pad: estructura a inicializar
 output	:	1: error. 0: exito.
 descr	:	setea los archivos del pad
 *******************************************************************/
uint8_t pad_setPathPADDefault(st_PAD *pad)
{
	if (pad == NULL)
	{
		g_showDebug ? printf("pad_setPathPAD: pad = null\n") : 0;
		return 1;
	}
	if (pad->pathDir == NULL)
	{
		g_showDebug ? printf("pad_setPathPAD: pathDir = null\n") : 0;
		return 1;
	}
	if (strlen(pad->pathDir) == 0)
	{
		g_showDebug ? printf("pad_setPathPAD: pathDir is empty\n") : 0;
		return 1;
	}
	if (pad->iFile[0].fileName == NULL || pad->iFile[1].fileName == NULL
			|| pad->iFile[2].fileName == NULL || pad->iFile[3].fileName == NULL)
	{
		g_showDebug ? printf("pad_setPathPAD: one fileName is null\n") : 0;
		return 1;
	}
	if (strlen(pad->iFile[0].fileName) == 0
			|| strlen(pad->iFile[1].fileName) == 0
			|| strlen(pad->iFile[2].fileName) == 0
			|| strlen(pad->iFile[3].fileName) == 0)
	{
		g_showDebug ? printf("pad_setPathPAD: one fileName is empty\n") : 0;
		return 1;
	}
	sprintf(pad->iFile[0].path, "%s/%s", pad->pathDir, pad->iFile[0].fileName);
	sprintf(pad->iFile[1].path, "%s/%s", pad->pathDir, pad->iFile[1].fileName);
	sprintf(pad->iFile[2].path, "%s/%s", pad->pathDir, pad->iFile[2].fileName);
	sprintf(pad->iFile[3].path, "%s/%s", pad->pathDir, pad->iFile[3].fileName);
	return 0;
}

/******************************************************************
 input	:	pad:  estructura a inicializar
 dest: donde se retornara el string.
 output	:	los nombres de los archivos del pad.
 descr	:	retorna los nombres de los archivos contenidos en el pad.
 Si dest == NULL, retorna "null".
 *******************************************************************/
char* pad_getFileNameArrayPAD(st_PAD *pad, char *dest)
{
	uint8_t i;
	uint8_t cursor = 0;
	char allNames[100];
	memset(allNames, 0, sizeof(allNames));

	for (i = 0; i < MAX_PAD_FILES; i++)
		cursor += sprintf(allNames + cursor, "%s\n", pad->iFile[i].fileName);
//		cursor += sprintf(dest + cursor, "%s\n", pad->iFile[i].fileName);
	if (g_showDebug)
		printf("%s - line %d: pad files:\n%s\n", __FILE__, __LINE__, allNames);

	if (dest != NULL)
	{
		sprintf(dest, allNames);
		return dest;
	}
	return "null";
}

/******************************************************************
 input	:	pad: estructura a inicializar
 output	:	nombre del archivo del pad.
 descr	:	retorna el nombre del archivo del pad segun el indice i.
 *******************************************************************/
char* pad_getFileNamePAD(st_PAD *pad, uint8_t i)
{
	return pad->iFile[i].fileName;
}

/******************************************************************
 input	:	pad: estructura a inicializar
 output	:	Id correspondiente al pad.
 descr	:	obtiene el id.
 *******************************************************************/
uint8_t pad_getIdPAD(st_PAD *pad)
{
	return pad->id;
}

/******************************************************************
 input	:	pad: estructura a inicializar
 output	:	Id correspondiente al pad.
 descr	:	obtiene el id.
 *******************************************************************/
char* pad_getString_IdPAD(st_PAD *pad)
{
	switch (pad->id)
	{
		case PAD_SNARE:
			return "PAD_SNARE";
		case PAD_KICK:
			return "PAD_KICK";
		case PAD_HITHAT:
			return "PAD_HITHAT";
		case PAD_TOM1:
			return "PAD_TOM1";
		case PAD_TOM2:
			return "PAD_TOM2";
		case PAD_TOM3:
			return "PAD_TOM3";
		case PAD_CYMBAL_L:
			return "PAD_CYMBAL_L";
		case PAD_CYMBAL_R:
			return "PAD_CYMBAL_R";
		default:
			return "NO_PAD";
	}
}

/****************************************************************************
 input	:	pad: estructura a inicializar
 output	:	exito = 0. error = 1.
 descr	:	verifica:
 * que el pad tenga apuntadores != NULL.
 * los directorios y nombre de los archivos no sean nulos o vacios
 *****************************************************************************/
uint8_t pad_verifyPAD(st_PAD *pad)
{
	if (pad == NULL)
	{
		g_showDebug ? printf("pad_setPathPAD: pad = null\n") : 0;
		return 1;
	}
	if (pad->pathDir == NULL)
	{
		g_showDebug ? printf("pad_setPathPAD: pathDir = null\n") : 0;
		return 1;
	}
	if (strlen(pad->pathDir) == 0)
	{
		g_showDebug ? printf("pad_setPathPAD: pathDir is empty\n") : 0;
		return 1;
	}
	if (pad->iFile[0].fileName == NULL || pad->iFile[1].fileName == NULL
			|| pad->iFile[2].fileName == NULL || pad->iFile[3].fileName == NULL)
	{
		g_showDebug ? printf("pad_setPathPAD: one fileName is null\n") : 0;
		return 1;
	}
	if (strlen(pad->iFile[0].fileName) == 0
			|| strlen(pad->iFile[1].fileName) == 0
			|| strlen(pad->iFile[2].fileName) == 0
			|| strlen(pad->iFile[3].fileName) == 0)
	{
		g_showDebug ? printf("pad_setPathPAD: one fileName is empty\n") : 0;
		return 1;
	}
	return 0;
}

/***********************************************************************
 input	:	pad : apuntador del tipo de estructura PAD.
 output	:	si hubo algun error retorna 1. De lo contrario 0.
 descr	:	lee la SD sin abrir ni hacer montaje ni desmontaje.
 Guarda la estructura FIL (SDFile).
 ************************************************************************/
uint8_t pad_openFilesPAD(st_PAD *pad)
{
	int i;
	if (!g_SDmntState)
	{
		g_showDebug ? printf("pad_openFilesPAD: SD is not mounted\n") : 0;
		return 1;
	}
	if (pad_verifyPAD(pad))
		return 1;
	for (i = 0; i < MAX_PAD_FILES; i++)
	{
		if (SD_openFile(pad->iFile[i].path, FA_READ | FA_WRITE) != FR_OK)
			return 1;
		pad->iFile[i].SD.fatFS = SDFatFS;
		pad->iFile[i].SD.file = SDFile;
		sprintf(pad->iFile[i].SD.path, "%.*s", strlen(SDPath), SDPath);
		g_showDebug ?
				printf("pad_openFiles: file opened: %s\n", pad->iFile[i].path) :
				0;
	}
	return 0;
}

/***********************************************************************
 input	:	pad : apuntador del tipo de estructura PAD.
 output	:	si hubo algun error retorna 1. De lo contrario 0.
 descr	:	cierra los archivos del pad.
 ************************************************************************/
uint8_t pad_closeFilesPAD(st_PAD *pad)
{
	if (pad->iFile[0].fileName == NULL || pad->iFile[1].fileName == NULL
			|| pad->iFile[2].fileName == NULL || pad->iFile[3].fileName == NULL)
	{
		g_showDebug ? printf("pad_closeFilesPAD: one fileName is null\n") : 0;
		return 1;
	}
	SD_closeFile(&pad->iFile[0].SD.file);
	SD_closeFile(&pad->iFile[1].SD.file);
	SD_closeFile(&pad->iFile[2].SD.file);
	SD_closeFile(&pad->iFile[3].SD.file);
	return 0;
}
/***********************************************************************
 input	:	pad : apuntador del tipo de estructura PAD.
 output	:	si hubo algun error retorna 1. De lo contrario 0.
 descr	:	lee la SD sin abrir ni hacer montaje ni desmontaje.
 Guarda la estructura FIL (SDFile).
 ************************************************************************/
uint8_t pad_readHeaderFilesPAD(st_PAD *pad)
{
	uint8_t i;
	FIL *SDFile;
	WAVE_format_typedef *file_header;

	for (i = 0; i < MAX_PAD_FILES; i++)
	{
		pad->lseek = 0;
		SDFile = &(pad->iFile[i].SD.file);
		file_header = &(pad->iFile[i].file.fileHeaderInfo);
		if (SD_readFile(SDFile, file_header, WAV_HEADER_SIZE, &(pad->lseek)))
		{
			g_showDebug ?
					printf(
							"pad_readHeaderFilesPAD: error en SD_readFile index = %d\n",
							i) :
					0;
			return 1;
		}
		printf("file %d: fileName = %s size = %lu\n", i, pad->iFile[i].fileName,
				file_header->FileSize);
	}
	pad_setDurationFilesPAD(pad);
	pad_setFormatAudioFilesPAD(pad);
	pad->indexFile = 3;
	g_showDebug = 1;
	g_showDebug ? pad_printHeaderFilePAD(pad) : 0;
	g_showDebug = 0;
	return 0;
}

/***********************************************************************
 input	:	pad : apuntador del tipo de estructura PAD.
 output	:	si hubo algun error retorna 1. De lo contrario 0.
 descr	:	lee la SD sin abrir ni hacer montaje ni desmontaje.
 Guarda la estructura FIL (SDFile).
 ************************************************************************/
uint8_t pad_setDurationFilesPAD(st_PAD *pad)
{
	if (pad == NULL)
		return 1;
	if (pad->iFile[0].fileName == NULL || pad->iFile[1].fileName == NULL
			|| pad->iFile[2].fileName == NULL || pad->iFile[3].fileName == NULL)
	{
		g_showDebug ?
				printf("pad_setDurationFilesPAD: one fileName is null\n") : 0;
		return 1;
	}
	WAVE_format_typedef *file_header =
			&(pad->iFile[pad->indexFile].file.fileHeaderInfo);
	uint32_t duration = file_header->FileSize / file_header->ByteRate;
	pad->iFile[0].file.duration = duration;
	pad->iFile[1].file.duration = duration;
	pad->iFile[2].file.duration = duration;
	pad->iFile[3].file.duration = duration;
	g_showDebug ?
			printf("pad_setDurationFilesPAD: fileSize = %lu, duration = %lu\n",
					file_header->FileSize, duration) :
			0;
	return 0;
}

/***********************************************************************
 input	:	pad : apuntador del tipo de estructura PAD.
 output	:	si hubo algun error retorna 1. De lo contrario 0.
 descr	:	lee la SD sin abrir ni hacer montaje ni desmontaje.
 Guarda la estructura FIL (SDFile).
 ************************************************************************/
uint8_t pad_setFormatAudioFilesPAD(st_PAD *pad)
{
	if (pad == NULL)
		return 1;
	if (pad->iFile[0].fileName == NULL || pad->iFile[1].fileName == NULL
			|| pad->iFile[2].fileName == NULL || pad->iFile[3].fileName == NULL)
	{
		g_showDebug ?
				printf("pad_setFormatAudioFilesPAD: one fileName is null\n") :
				0;
		return 1;
	}
	pad->iFile[0].file.fileFormat = WAV_FORMAT;
	pad->iFile[1].file.fileFormat = WAV_FORMAT;
	pad->iFile[2].file.fileFormat = WAV_FORMAT;
	pad->iFile[3].file.fileFormat = WAV_FORMAT;
	return 0;
}

/***********************************************************************
 input	:	pad : apuntador del tipo de estructura PAD.
 output	:	si hubo algun error retorna 1. De lo contrario 0.
 descr	:	lee la SD sin abrir ni hacer montaje ni desmontaje.
 Guarda la estructura FIL (SDFile).
 ************************************************************************/
void pad_printHeaderFilePAD(st_PAD *pad)
{
	uint8_t i = pad->indexFile;
	WAVE_format_typedef *file_header = &(pad->iFile[i].file.fileHeaderInfo);

	printf("     WAVE File Header\n");
	printf("ChunkID       = %lu\n", file_header->ChunkID);
	printf("FileSize      = %lu bytes\n", file_header->FileSize);
	printf("SampleRate    = %lu hz\n", file_header->SampleRate);
	printf("BitPerSample  = %d\n", file_header->BitPerSample);
	printf("ByteRate      = %lu byte/s\n", file_header->ByteRate);
	printf("FileFormat    = 0x%lx\n", file_header->FileFormat);
	printf("NbrChannels   = %d\n", file_header->NbrChannels);
	printf("duration      = %lu seg\n", pad->iFile[i].file.duration);
	printf("SubChunk1ID   = %lu\n", file_header->SubChunk1ID);
	printf("SubChunk1Size = %lu\n", file_header->SubChunk1Size);
	printf("AudioFormat   = %d\n", file_header->AudioFormat);
	printf("BlockAlign    = %d\n", file_header->BlockAlign);
	printf("SubChunk2ID   = %lu\n", file_header->SubChunk2ID);
	printf("SubChunk2Size = %lu\n", file_header->SubChunk2Size);
}

/***********************************************************************
 input	:	pad : apuntador del tipo de estructura PAD.
 output	:	si hubo algun error retorna 1. De lo contrario 0.
 descr	:	lee la SD sin abrir ni hacer montaje ni desmontaje.
 Guarda la estructura FIL (SDFile).
 ************************************************************************/
uint8_t pad_readFilePAD(st_PAD *pad)
{
	if (pad->buffer == NULL)
	{
		g_showDebug ? printf("pad_readFilePAD: buffer null\n") : 0;
		return 1;
	}

	uint8_t i = pad->indexFile;
	FIL *SDFile = &(pad->iFile[i].SD.file);
	int32_t length = pad_verifyTopFilePAD(pad); //DMA_I2S_READ_FILE_SIZE;//

	if (length <= 0)
	{
		pad->lseek = WAV_HEADER_SIZE;
		length = DMA_I2S_READ_FILE_SIZE;
	}

	if (SD_readFile(SDFile, pad->buffer, length, &(pad->lseek)))
	{
		g_showDebug ? printf("pad_readFilePAD: error en SD_readFile\n") : 0;
		return 1;
	}

	g_showDebug ?
			printf("pad_readFilePAD: it's ok. lseek = %lu\n", pad->lseek) : 0;
	return 0;
}

/***********************************************************************
 input	:	pad : apuntador del tipo de estructura PAD.
 output	:	DMA_I2S_BUFFER_SIZE: si aun no llega al tope del archivo.
 < DMA_I2S_BUFFER_SIZE: resto del archivo.
 descr	:	lee la SD sin abrir ni hacer montaje ni desmontaje.
 Guarda la estructura FIL (SDFile).
 ************************************************************************/
int32_t pad_verifyTopFilePAD(st_PAD *pad)
{
	uint8_t i = pad->indexFile;
	WAVE_format_typedef *file_header = &(pad->iFile[i].file.fileHeaderInfo);
	int32_t r = file_header->FileSize - pad->lseek;

	if (r >= DMA_I2S_READ_FILE_SIZE)
	{
		g_showDebug ?
				printf("verifyTopFilePad: r = %ld, lseek = %lu\n", r,
						pad->lseek) :
				0;
		return DMA_I2S_READ_FILE_SIZE;
	}
	else if (r <= 0)
		return 0;
	g_showDebug ?
			printf("verifyTopFilePad: r = %ld, lseek = %lu\n", r, pad->lseek) :
			0;
	return r;
}

