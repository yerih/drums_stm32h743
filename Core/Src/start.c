/*******************************************************************************
 * @file           : start.c
 * @brief          : inicio de la aplicacion
 * @author			: Yerih Iturriago
 ******************************************************************************/

#include "start.h"
extern DSTATUS SD_initialize(BYTE);
extern SD_HandleTypeDef hsd1;
//#define ALGO_
#ifdef ALGO_

extern osThreadId_t threadMain_id;//defaultTaskHandle;
extern const osThreadAttr_t threadMain_attributes;

/**
  * @brief  inicia el hilo principal
  * @param  None
  * @retval None
  */
void start_init_threadMain(void) {

  threadMain_id = osThreadNew(start_threadMain_function, NULL, &threadMain_attributes);

}


void start_threadMain_function(void *argument)
{
	while(1)
	{
		HAL_Delay(300);
		LED_TOGGLE_D3;
		printf("Drum working in threadMain: testing fatFS\n");
	}
}

#endif


/**
  * @brief  inicia el hilo principal
  * @param  None
  * @retval None
  */
void start_threadMain_function(void *argument)
{
	start_initialization();
}



/**
  * @brief  iniciacializa el sistema
  * @param  None
  * @retval None
  */
void start_initialization(void)
{
	//SD initialization
	SD_init();
	pad_initPADdefault(&pad_snare);
	printf("format bit: %d\n", SAMPLE_BIT);
	printf("path: %s\n", pad_snare.iFile[2].path);
	printf("size file: %lu bytes\n", pad_snare.iFile[2].file.fileHeaderInfo.FileSize);

	//audio initialization
	audio_init();
//	audio_playWav(&pad_snare.iFile[2].SD.file, 100);
	uint8_t res = f_open(&SDFile, "sine.wav", FA_READ);
	if(res != FR_OK)
	{
		char a[50];
		memset(a, 0, sizeof(a));
		printf("error opening: %s\n", utils_fresult(res, a));
		return;
	}
//	audio_readFile(&SDFile, audioBuffer.M0, &pad_snare.lseek);
	audio_playWav(&SDFile, 1);

//	char a[50];
//	DMA_I2S_FORMATBIT buffer[DMA_I2S_BUFFER_SIZE];
//	memset(a, 0, sizeof(a));
//	memset(buffer, 0, sizeof(buffer));
//	uint8_t res;
//	uint32_t lseek;
//
//	res = f_open(&SDFile, "/audiolib/default/snare/snare_124.wav", FA_READ);
//	if(res != FR_OK)
//	{
//		printf("error opening: %s\n", utils_fresult(res, a));
//	}
//	audio_readFile(&SDFile, buffer, lseek);
}
