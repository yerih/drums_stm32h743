/*******************************************************************************
  * @file           : global.c
  * @brief          : incluye todas las librerias y variables globales
  * @author			: Yerih Iturriago
  ******************************************************************************/

#include "global.h"



int g_showDebug = FALSE;
int g_SDmntState = FALSE;//indica si una sd esta montada con sd_mount()
extern st_PAD pad_snare;

#if !defined(ONLY_SNARE) || defined(WITH_KICK)
extern st_PAD pad_kick;
#endif

#if !defined(ONLY_SNARE) || defined(WITH_HITHAT)
extern st_PAD pad_hithat;
#endif

#ifndef ONLY_SNARE
extern st_PAD pad_tom1;
extern st_PAD pad_tom2;
extern st_PAD pad_tom3;
extern st_PAD pad_cymbal_L;
extern st_PAD pad_cymbal_R;
#endif

/****************************************************************
 * params: sequence: LED_SEQ_THREAD: secuencia para hilos
 * return: nothing
 * descrp: enciende y apaga leds generando una secuencia.
 ****************************************************************/
void led_sequence(int sequence)
{
	int i = 0;
	uint32_t delay;

	switch(sequence)
	{
		case LED_SEQ_ERROR:   delay = 50;  break;
		case LED_SEQ_SUCCESS: delay = 500; break;
		case LED_SEQ_THREAD:  delay = 100; break;
		default: delay = 300; break;
	}

	switch(sequence)
	{
		case LED_SEQ_THREAD:  	while(1)
								{
									for(int i = 0;i < 3; i++)
									{
										LED_TOGGLE_D3;
										HAL_Delay(delay);
									}
									for(int i = 0;i < 3; i++)
									{
										LED_TOGGLE_D3;
										HAL_Delay(1000);
									}
								}
								break;

		case LED_SEQ_ERROR:		for(i = 0; i < 100; i++)
								{
									LED_TOGGLE_D3;
//									osDelay(delay);
									HAL_Delay(delay);
								}
								break;
		default:
								for(i = 0; i < 100; i++)
								{
									LED_TOGGLE_D3;
									HAL_Delay(delay);
								}
								break;
	}


}

extern DSTATUS SD_initialize(BYTE lun);
