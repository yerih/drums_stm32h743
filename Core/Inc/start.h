/*******************************************************************************
  * @file           : start.h
  * @brief          : inicio de la aplicacion
  * @author			: Yerih Iturriago
  ******************************************************************************/

#ifndef START_H_
#define START_H_

#include "global.h"

/**
  * @brief  hilo principal
  * @param  None
  * @retval None
  */
void start_init_threadMain(void);


/**
  * @brief  iniciacializa el sistema
  * @param  None
  * @retval None
  */
void start_initialization(void);


#endif

