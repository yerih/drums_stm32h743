/*******************************************************************************
  * @file           : utils.h
  * @brief          : utilidades
  * @author			: Yerih Iturriago
  * @note			:
  *
  ******************************************************************************/

#ifndef UTILS_H_
#define UTILS_H_

#include "global.h"



/******************************************************************
function: 	utils_dresult
input	:	dLabel: referido al enum DRESULT.
output	:	string de datos
descr	:	retorna el string del error segun frLabel.
*******************************************************************/
char* utils_dresult(uint8_t dLabel, char* str);

/******************************************************************
function: 	utils_fresult
input	:	frLabel: referido al enum FRESULT.
output	:	string de datos
descr	:	retorna el string del error segun frLabel.
*******************************************************************/
char* utils_fresult(uint8_t frLabel, char* str);



#endif
