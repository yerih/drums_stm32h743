/*******************************************************************************
  * @file           : audio.h
  * @brief          : manejo de audio
  * @author			: Yerih Iturriago
  * @note			:
  *
  ******************************************************************************/


#ifndef AUDIOPLAY_H_
#define AUDIOPLAY_H_

#include "global.h"

extern st_AUDIO_BUFFER audioBuffer;



/**
  * @brief  iniciacializa el motor de audio
  * @param  None
  * @retval None
  */
void audio_init();



/**
  * @brief  reproduce un archivo wav previamente abierto.
  * @param  file:  archivo abierto.
  * @param	power: multiplicador de valor. Se usa para amplitud de onda.
  * @retval None
  */
void audio_playWav(FIL* file, float power);


/**
  * @brief  lee un archivo wav de la SD previamente abierto.
  * @param  file:  archivo abierto.
  * @retval None
  */
int audio_readFile(FIL* file, void* buffer, uint32_t* lseek);

/***********************************************************************
 input	:	pad : apuntador del tipo de estructura PAD.
 output	:	DMA_I2S_BUFFER_SIZE: si aun no llega al tope del archivo.
 < DMA_I2S_BUFFER_SIZE: resto del archivo.
 descr	:	lee la SD sin abrir ni hacer montaje ni desmontaje.
 Guarda la estructura FIL (SDFile).
 ************************************************************************/
int32_t audio_verifyTopFile(uint32_t fsizeData, uint32_t lseek);


/**
  * @brief  mezcla en buffer segun la potencia o volumen
  * @param  dest: direccion de destino
  * @param  src:  direccion de fuente
  * @param  len:  tamaño de la transferencia
  * @param  power:  multiplicador de valor. Se usa para volument o potencia
  * @retval 1: file finish read, -1: error, 0: success.
  */
void mix(DMA_I2S_FORMATBIT* dest, DMA_I2S_FORMATBIT* src, uint32_t len, float power);

#endif

