/*******************************************************************************
  * @file           : global.h
  * @brief          : incluye todas las librerias y variables globales
  * @author			: Yerih Iturriago
  ******************************************************************************/

#ifndef PAD_DEF_H_
#define PAD_DEF_H_



#define PAD_PATH_AUDIO_LIB	"audiolib"
#define PAD_PATH_DEFAULT 	"default"
#define PAD_PATH_SNARE	 	"snare"
#define PAD_PATH_KICK	 	"kick"
#define PAD_PATH_HITHAT	 	"hithat"
#define PAD_PATH_TOM1	 	"tom1"
#define PAD_PATH_TOM2	 	"tom2"
#define PAD_PATH_TOM3		"tom3"
#define PAD_PATH_CYMBAL_L	"cymbal_L"
#define PAD_PATH_CYMBAL_R	"cymbal_R"

enum num_PAD
{
	NO_PAD = 0,
	PAD_SNARE,
	PAD_KICK,
	PAD_HITHAT,
	PAD_TOM1,
	PAD_TOM2,
	PAD_TOM3,
	PAD_CYMBAL_L,
	PAD_CYMBAL_R
};



enum
{
	WAV_FORMAT = 0,
	MP3_FORMAT,
	AAC_FORMAT,
	WMA_FORMAT,
};


enum fmtNameByIntensity
{
	FMT_HYPHEN_NUMBER = 0,
	FMT_USCORP_NUMBER,
	FMT_ONLY_NUMBER,
};


/* Estructura de estado de audio */
typedef enum num_Audio_state
{
	AUDIO_INACTV = 0,
	AUDIO_ACTV,
	AUDIO_PARCIAL,
} AUDIO_State;

enum num_playPad
{
	MUTE_PAD = 0,
	PLAY_PAD,
};

typedef struct st_WAVE_Format
{
	uint32_t ChunkID; 		/* 0 */
	uint32_t FileSize; 		/* 4 */
	uint32_t FileFormat; 	/* 8 */
	uint32_t SubChunk1ID; 	/* 12 */
	uint32_t SubChunk1Size; /* 16 */
	uint16_t AudioFormat; 	/* 20 */
	uint16_t NbrChannels; 	/* 22 */
	uint32_t SampleRate; 	/* 24 */
	uint32_t ByteRate; 		/* 28 */
	uint16_t BlockAlign; 	/* 32 */
	uint16_t BitPerSample; 	/* 34 */
	uint32_t SubChunk2ID; 	/* 36 */
	uint32_t SubChunk2Size; /* 40 */

} WAVE_format_typedef;


typedef struct st_SD_fatfs
{
	uint8_t ret;     /* Return value for SD */
	char    path[4]; /* SD logical drive path */
	FATFS   fatFS;   /* File system object for SD logical drive */
	FIL     file;    /* File object for SD */

}st_SD_typedef;

typedef struct st_AUDIO_FILE
{
	uint8_t 				fileID;
	uint8_t 				fileFormat;
	//char					prevPath[50];
	uint32_t	   			duration;
	uint32_t			 	bytesRead;
	WAVE_format_typedef     fileHeaderInfo;

} AUDIO_file_st_typedef;

typedef struct st_FILES_MANAGE
{
	AUDIO_file_st_typedef	file;
	st_SD_typedef		  	SD;
	char					path[120];
	char 	   			    fileName[50];
}st_FILES_MANAGE;


typedef struct st_AUDIO_PAD
{
	st_FILES_MANAGE			iFile[MAX_PAD_FILES];		//gestor de archivos abiertos.
	DMA_I2S_FORMATBIT     	*buffer;		//audio buffer principal.
	DMA_I2S_FORMATBIT     	*prevBuff;		//audio buffer del audio previo.
	char 	   			    pathDir[50];	//directorio dentro de la carpeta audiolib
	float				  	vol;			//Volumen general.
	float				  	volFile;		//volumen del archivo.
	uint8_t					id;
	uint8_t 			  	intensidad;		//intensidad del ADC.
	uint8_t				  	actv;			//activar o desactivar.
	uint8_t					tcnt;			//contador de timer.
	uint8_t					hit;			//indica si se golpeo.
	uint8_t					playPad;		//indica si se esta reproduciendo o modo playing.
	uint8_t					indexFile;		//indica el indice de iFile[].
	uint32_t				lseek;			//cursor de archivo iFile[indexFile].
} st_PAD;



//Buffer para pads
extern DMA_I2S_FORMATBIT padBuffer_MIX[DMA_I2S_BUFFER_SIZE];
//extern DMA_I2S_FORMATBIT padBuffer_SNARE[DMA_I2S_BUFFER_SIZE];
extern DMA_I2S_FORMATBIT padBuffer_SNARE[1];
extern DMA_I2S_FORMATBIT padBuffer_SNAREprev[DMA_I2S_BUFFER_SIZE];
#if	!defined(ONLY_SNARE) || defined(WITH_KICK)
extern DMA_I2S_FORMATBIT padBuffer_KICK[DMA_I2S_BUFFER_SIZE];
extern DMA_I2S_FORMATBIT padBuffer_KICKprev[DMA_I2S_BUFFER_SIZE];
#endif
#if !defined(ONLY_SNARE) || defined(WITH_HITHAT)
extern DMA_I2S_FORMATBIT padBuffer_HITHAT[DMA_I2S_BUFFER_SIZE];
extern DMA_I2S_FORMATBIT padBuffer_HITHATprev[DMA_I2S_BUFFER_SIZE];
#endif
#ifndef ONLY_SNARE
extern DMA_I2S_FORMATBIT padBuffer_TOM1[DMA_I2S_BUFFER_SIZE];
extern DMA_I2S_FORMATBIT padBuffer_TOM1prev[DMA_I2S_BUFFER_SIZE];
extern DMA_I2S_FORMATBIT padBuffer_TOM2[DMA_I2S_BUFFER_SIZE];
extern DMA_I2S_FORMATBIT padBuffer_TOM2prev[DMA_I2S_BUFFER_SIZE];
extern DMA_I2S_FORMATBIT padBuffer_TOM3[DMA_I2S_BUFFER_SIZE];
extern DMA_I2S_FORMATBIT padBuffer_TOM3prev[DMA_I2S_BUFFER_SIZE];
extern DMA_I2S_FORMATBIT padBuffer_CYMBAL_L[DMA_I2S_BUFFER_SIZE];
extern DMA_I2S_FORMATBIT padBuffer_CYMBAL_Lprev[DMA_I2S_BUFFER_SIZE];
extern DMA_I2S_FORMATBIT padBuffer_CYMBAL_R[DMA_I2S_BUFFER_SIZE];
extern DMA_I2S_FORMATBIT padBuffer_CYMBAL_Rprev[DMA_I2S_BUFFER_SIZE];
#endif

//----------------------------------------------- PADs
extern st_PAD pad_snare;

#if !defined(ONLY_SNARE) || defined(WITH_KICK)
extern st_PAD pad_kick;
#endif

#if !defined(ONLY_SNARE) || defined(WITH_HITHAT)
extern st_PAD pad_hithat;
#endif

#ifndef ONLY_SNARE
extern st_PAD pad_tom1;
extern st_PAD pad_tom2;
extern st_PAD pad_tom3;
extern st_PAD pad_cymbal_L;
extern st_PAD pad_cymbal_R;
#endif


#endif



