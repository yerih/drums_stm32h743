/*******************************************************************************
  * @file           : threads.h
  * @brief          : contiene los hilos del app
  * @author			: Yerih Iturriago
  ******************************************************************************/
#ifndef THREADS_H_
#define THREADS_H_

#include "global.h"

void start_threadMain_function(void *argument);


#endif
