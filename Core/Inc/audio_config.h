/*******************************************************************************
  * @file           : audio_config.h
  * @brief          : configuracion para manejo de audio
  * @author			: Yerih Iturriago
  * @note			:
  *
  ******************************************************************************/

#ifndef AUDIO_CONFIG_H_
#define	AUDIO_CONFIG_H_

//#include "global.h"
#define NUMBER_CHANNELS				2
#define MAX_PAD_FILES				4


/* Descomentar segun su uso */
//#define SAMPLES_32_BIT			32
//#define SAMPLES_24_BIT			24
#define SAMPLES_16_BIT			16
//#define SAMPLES_8_BIT				8

#ifdef	SAMPLES_32_BIT
#define SAMPLE_BIT					32
#define DMA_I2S_BUFFER_SIZE			256*NUMBER_CHANNELS		//tamano de samples de 32bits. Ejemplo: 256*32bits = 1024 bytes. 256 word  (24 bits, 3 bytes).
#define DMA_I2S_FORMATBIT			int32_t//float//int32_t//uint32_t
#define DMA_I2S_BYTES_MULTIPLIER	4
#define DMA_I2S_READ_FILE_SIZE		DMA_I2S_BUFFER_SIZE*DMA_I2S_BYTES_MULTIPLIER
#ifdef	SINEWAVE_TEST
#define SINEWAVE_32b
#endif
#endif


#ifdef	SAMPLES_24_BIT
#define SAMPLE_BIT					24
#define DMA_I2S_BUFFER_SIZE			256*NUMBER_CHANNELS			//tamano de samples de 24bits. Ejemplo: 341*24bits/8 = 1023 bytes. 341 3/4word  (24 bits, 3 bytes).
#define DMA_I2S_FORMATBIT			int32_t//uint32_t
#define DMA_I2S_BYTES_MULTIPLIER	3			//Deberia ser 3 pero como no existe uint24_t, se usan elementos de uint32_t.
#define DMA_I2S_READ_FILE_SIZE		DMA_I2S_BUFFER_SIZE*DMA_I2S_BYTES_MULTIPLIER
#ifdef	SINEWAVE_TEST
#define SINEWAVE_24b
#endif
#endif


#ifdef	SAMPLES_16_BIT
#define SAMPLE_BIT					16
#define DMA_I2S_BUFFER_SIZE		 	32*NUMBER_CHANNELS//1//32//64//128//512		//tamano de samples de 16bits. Ejemplo: 512*16bits/8 = 1024 bytes. 512 halfword (16 bits, 2 bytes).
#define DMA_I2S_BYTES_MULTIPLIER	2
#define DMA_I2S_READ_FILE_SIZE		DMA_I2S_BUFFER_SIZE*DMA_I2S_BYTES_MULTIPLIER //bytes
#define DMA_I2S_FORMATBIT			int16_t//uint16_t//
#ifdef	SINEWAVE_TEST
#define SINEWAVE_16b
#define SINEWAVE_16b_VIEJO
//#define SINEWAVE_16b_NUEVO
#endif
#endif


#ifdef	SAMPLES_8_BIT
#define SAMPLE_BIT					8
#define DMA_I2S_BUFFER_SIZE		    1024*NUMBER_CHANNELS	//tamano de samples de  8bits.
#define DMA_I2S_FORMATBIT			int8_t//uint8_t
#define DMA_I2S_BYTES_MULTIPLIER	1
#define DMA_I2S_READ_FILE_SIZE		DMA_I2S_BUFFER_SIZE*DMA_I2S_BYTES_MULTIPLIER
#ifdef	SINEWAVE_TEST
#define SINEWAVE_8b
#endif
#endif


/* Definición de estado de audio */
#define AUDIO_OK					0
#define AUDIO_ERROR					1
#define AUDIO_TIMEOUT 				2


/* MUTE comandos */
#define AUDIO_MUTE_ON  				1
#define AUDIO_MUTE_OFF 				0

//WAV files
#define WAV_HEADER_SIZE				44
#define WAV_DATA_START				WAV_HEADER_SIZE



typedef struct st_CIRCULAR_AUDIO_BUFFER
{
		DMA_I2S_FORMATBIT M0[DMA_I2S_BUFFER_SIZE];
		DMA_I2S_FORMATBIT M1[DMA_I2S_BUFFER_SIZE];
		HAL_DMA_MemoryTypeDef memAvailable;
}st_AUDIO_BUFFER;

typedef enum {
	PUT_IN_M0 = 0,
	PUT_IN_M1 = 1,
	WAITING	  = 2
}enum_AUDIO_BUF_OPERATION;


#endif






