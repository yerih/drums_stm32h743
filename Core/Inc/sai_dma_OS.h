/*******************************************************************************
  * @file           : sai_dma_OS.h
  * @brief          : gestion del modulo SAI y su stream DMA asignado
  * @author			: Yerih Iturriago
  ******************************************************************************/

#ifndef SAI_DMA_OS_H_
#define SAI_DMA_OS_H_


#include "global.h"
#include "stm32h7xx_hal.h"
#include "stm32h7xx_hal_sai.h"
#include "stm32h7xx_hal_i2s.h"


typedef enum
{
  SAI_MODE_DMA,
  SAI_MODE_IT
} SAI_ModeTypedef;
/**
  * @brief  Transmit an amount of data in non-blocking mode with DMA.
  * @param  hsai pointer to a SAI_HandleTypeDef structure that contains
  *              the configuration information for SAI module.
  * @param  pData Pointer to data buffer
  * @param  Size Amount of data to be sent
  * @retval HAL status
  */
HAL_StatusTypeDef HAL_SAI_Transmit_DMA_OS(SAI_HandleTypeDef *hsai, uint8_t *pData, uint16_t Size);

/**
  * @brief  inicializa sai dma
  * @param  hdma pointer to a DMA_HandleTypeDef structure that contains
  *              the configuration information for the specified DMA module.
  * @retval None
  */
void sai_DMA_init(st_AUDIO_BUFFER* audioBuffer);

/**
  * @brief  inicializa i2s dma
  * @param  hdma pointer to a DMA_HandleTypeDef structure that contains
  *              the configuration information for the specified DMA module.
  * @retval None
  */
void i2s_DMA_init(st_AUDIO_BUFFER* audioBuffer);

/**
  * @brief  Transmit an amount of data in non-blocking mode with DMA
  * @param  hi2s pointer to a I2S_HandleTypeDef structure that contains
  *         the configuration information for I2S module
  * @param  pData a 16-bit pointer to the Transmit data buffer.
  * @param  Size number of data sample to be sent:
  * @note   When a 16-bit data frame or a 16-bit data frame extended is selected during the I2S
  *         configuration phase, the Size parameter means the number of 16-bit data length
  *         in the transaction and when a 24-bit data frame or a 32-bit data frame is selected
  *         the Size parameter means the number of 16-bit data length.
  * @note   The I2S is kept enabled at the end of transaction to avoid the clock de-synchronization
  *         between Master and Slave(example: audio streaming).
  * @retval HAL status
  */
HAL_StatusTypeDef HAL_I2S_Transmit_DMA_OS(I2S_HandleTypeDef *hi2s, uint16_t *pData, uint16_t Size);


#endif
