/*******************************************************************************
  * @file           : sd_os.h
  * @brief          : manipulacion de archivo usando SD
  * @author			: Yerih Iturriago
  * @note			: Se debe configurar las definiciones : VOLUME_DRIVER, SDFatFS, SDPath, SDFile
  *
  ******************************************************************************/

#ifndef SD_OS_H_
#define SD_OS_H_

#include "global.h"


extern uint8_t retSD;    /* Return value for SD */
extern char SDPath[4];   /* SD logical drive path */
extern FATFS SDFatFS;    /* File system object for SD logical drive */
extern FIL SDFile;       /* File object for SD */


/******************************************************************
function: 	SD_mount
input	:	show: TRUE: muestra msj en pantalla
output	:	TRUE: si hubo algun error.
descr	:	realiza montaje de SD. Se debe llamar a SD_unmount().
			despues de realizar todas operaciones con la SD.
*******************************************************************/
uint8_t SD_mount(void);

/******************************************************************
function: 	SD_mount
input	:	show: TRUE: muestra msj en pantalla
output	:	TRUE: si hubo algun error.
descr	:	realiza montaje de SD. Se debe llamar a SD_unmount().
			despues de realizar todas operaciones con la SD.
*******************************************************************/
uint8_t SD_unmount(void);

/******************************************************************
input	:	nothing
output	:	TRUE: si hubo algun error.
descr	:	realiza montaje de SD. Se debe llamar a SD_unmount().
			despues de realizar todas operaciones con la SD.
*******************************************************************/
uint8_t SD_init(void);

/******************************************************************
function: 	SD_openFile
input	:	mode: WRITE: crea el archivo y lo edita.
				  READ : lee el archivo.

output	:	si hubo algun error retorna 1. De lo contrario 0.
descr	:	Abre un archivo de la SD segun el modo. Se debe
			realizar montaje de SD con SD_mount. Se debe cerrar el
			archivo luego de usarlo.
*******************************************************************/
uint8_t SD_openFile(char* fileName, uint8_t mode);

/******************************************************************
function: 	SD_closeFile
input	:	SDFile: apuntador hacia el archivo SD.
output	:	si hubo algun error retorna 1. De lo contrario 0.
descr	:	Cierra el archivo FIL de la SD.
*******************************************************************/
uint8_t SD_closeFile(FIL *SDFile);

/***********************************************************************
function: 	SD_writeFile
input	:	fileName: nombre del archivo con su extension.
			buffer  : direccion donde se almacenará la data leida.
			len		: tmno del buffer o de la data.
output	:	si hubo algun error retorna 1. De lo contrario 0.
descr	:	Abre un archivo de la SD en modo FA_READ o FA_OPEN_EXISTING.
************************************************************************/
uint8_t SD_writeFile(char* fileName, void* buffer, uint32_t len);


/***********************************************************************
function: 	SD_readFile
input	:	fileName: nombre del archivo con su extension.
			buffer  : direccion donde se almacenará la data leida.
			len		: tmno del buffer o de la data.
			*lseek  : apuntador hacia el cursor u offset de R/W del
					  archivo.
output	:	si hubo algun error retorna 1. De lo contrario 0.
descr	:	Abre un archivo de la SD en modo FA_READ o FA_OPEN_EXISTING
			y lo cierra.
************************************************************************/
uint8_t SD_readFile(FIL* SDFile, void* buffer, uint32_t len, uint32_t* lseek);


/***********************************************************************
function: 	SD_lseek
input	:	fileName    : nombre del archivo con su extension.
			lseek       : cursor u offset de R/W del archivo.
output	:	si hubo algun error retorna 1. De lo contrario 0.
descr	:	establece el lseek o cursor de R/W del archivo.
************************************************************************/
uint8_t SD_lseek(char* fileName, uint32_t lseek);







#endif




