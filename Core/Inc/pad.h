/*******************************************************************************
  * @file           : pad.h
  * @brief          : manejo de pads
  * @author			: Yerih Iturriago
  * @note			:
  ******************************************************************************/



#ifndef PAD_H_
#define PAD_H_

#include "global.h"




/******************************************************************
input	:	pad: estructura a inicializar
output	:	nothing
descr	:	inicializa un pad.
*******************************************************************/
void pad_initPAD(st_PAD* pad);

/******************************************************************
input	:	pad: estructura a inicializar
output	:	nothing
descr	:	inicializa un pad con archvos default.
*******************************************************************/
void pad_initPADdefault(st_PAD* pad);

/******************************************************************
input	:	pad: estructura a inicializar
output	:	Id correspondiente al pad.
descr	:	setea el Id.
*******************************************************************/
uint8_t pad_setIdPAD(st_PAD* pad);

/******************************************************************
input	:	pad: estructura a inicializar
output	:	1: error. 0: exito
descr	:	setea el buffer del pad.
*******************************************************************/
uint8_t pad_setBufferPAD(st_PAD* pad);

/******************************************************************
input	:	pad: estructura a inicializar
output	:	1: error. 0: exito
descr	:	setea el Id.
*******************************************************************/
DMA_I2S_FORMATBIT* pad_getBufferPAD(st_PAD* pad);

/******************************************************************
input	:	pad: estructura a inicializar
output	:	1: error. 0: exito.
descr	:	setea el directorio del pad
*******************************************************************/
uint8_t pad_setDirPADDefault(st_PAD* pad);

/******************************************************************
input	:	pad: estructura a inicializar
output	:	1: error. 0: exito.
descr	:	setea el directorio del pad
*******************************************************************/
uint8_t pad_setDirPAD(st_PAD* pad, char* pathDir);

/******************************************************************
input	:	pad: estructura a inicializar
output	:	1: error. 0: exito.
descr	:	setea los archivos del pad
*******************************************************************/
uint8_t pad_setFilePADDefault(st_PAD* pad);

/******************************************************************
input	:	pad: estructura a inicializar
output	:	1: error. 0: exito.
descr	:	setea los archivos del pad
*******************************************************************/
uint8_t pad_setPathPADDefault(st_PAD* pad);

/******************************************************************
input	:	pad: estructura a inicializar
output	:	nothing
descr	:	imprime los nombres de los arhivos del pad
*******************************************************************/
void pad_printFileNameArrayPAD(st_PAD* pad);

/******************************************************************
input	:	pad:  estructura a inicializar
			dest: donde se retornara el string.
output	:	los nombres de los archivos del pad.
descr	:	retorna los nombres de los archivos contenidos en el pad.
			Si dest == NULL, retorna "null".
*******************************************************************/
char* pad_getFileNameArrayPAD(st_PAD* pad, char* dest);

/******************************************************************
input	:	pad: estructura a inicializar
output	:	nombre del archivo del pad.
descr	:	retorna el nombre del archivo del pad segun el indice i.
*******************************************************************/
char* pad_getFileNamePAD(st_PAD* pad, uint8_t i);

/******************************************************************
input	:	pad: estructura a inicializar
output	:	Id correspondiente al pad.
descr	:	inicializa un pad.
*******************************************************************/
uint8_t audio_getIdPAD(st_PAD* pad);


/******************************************************************
input	:	pad: estructura a inicializar
output	:	Id correspondiente al pad.
descr	:	obtiene el id.
*******************************************************************/
char* pad_getString_IdPAD(st_PAD* pad);

/****************************************************************************
input	:	pad: estructura a inicializar
output	:	exito = 0. error = 1.
descr	:	verifica:
			* que el pad tenga apuntadores != NULL.
			* los directorios y nombre de los archivos no sean nulos o vacios
*****************************************************************************/
uint8_t pad_verifyPAD(st_PAD* pad);

/***********************************************************************
input	:	pad : apuntador del tipo de estructura PAD.
output	:	si hubo algun error retorna 1. De lo contrario 0.
descr	:	lee la SD sin abrir ni hacer montaje ni desmontaje.
			Guarda la estructura FIL (SDFile).
************************************************************************/
uint8_t pad_openFilesPAD(st_PAD *pad);

/***********************************************************************
input	:	pad : apuntador del tipo de estructura PAD.
output	:	si hubo algun error retorna 1. De lo contrario 0.
descr	:	cierra los archivos del pad.
************************************************************************/
uint8_t pad_closeFilesPAD(st_PAD *pad);

/***********************************************************************
input	:	pad : apuntador del tipo de estructura PAD.
output	:	si hubo algun error retorna 1. De lo contrario 0.
descr	:	lee la SD sin abrir ni hacer montaje ni desmontaje.
			Guarda la estructura FIL (SDFile).
************************************************************************/
uint8_t pad_readHeaderFilesPAD(st_PAD* pad);

/***********************************************************************
input	:	pad : apuntador del tipo de estructura PAD.
output	:	si hubo algun error retorna 1. De lo contrario 0.
descr	:	lee la SD sin abrir ni hacer montaje ni desmontaje.
			Guarda la estructura FIL (SDFile).
************************************************************************/
uint8_t pad_setDurationFilesPAD(st_PAD* pad);

/***********************************************************************
input	:	pad : apuntador del tipo de estructura PAD.
output	:	si hubo algun error retorna 1. De lo contrario 0.
descr	:	lee la SD sin abrir ni hacer montaje ni desmontaje.
			Guarda la estructura FIL (SDFile).
************************************************************************/
uint8_t pad_setFormatAudioFilesPAD(st_PAD* pad);

/***********************************************************************
input	:	pad : apuntador del tipo de estructura PAD.
output	:	si hubo algun error retorna 1. De lo contrario 0.
descr	:	lee la SD sin abrir ni hacer montaje ni desmontaje.
			Guarda la estructura FIL (SDFile).
************************************************************************/
void pad_printHeaderFilePAD(st_PAD* pad);

/***********************************************************************
input	:	pad : apuntador del tipo de estructura PAD.
output	:	si hubo algun error retorna 1. De lo contrario 0.
descr	:	lee la SD sin abrir ni hacer montaje ni desmontaje.
			Guarda la estructura FIL (SDFile).
************************************************************************/
uint8_t pad_readFilePAD(st_PAD *pad);

/***********************************************************************
input	:	pad : apuntador del tipo de estructura PAD.
output	:	DMA_I2S_BUFFER_SIZE: si aun no llega al tope del archivo.
			< DMA_I2S_BUFFER_SIZE: resto del archivo.
descr	:	lee la SD sin abrir ni hacer montaje ni desmontaje.
			Guarda la estructura FIL (SDFile).
************************************************************************/
int32_t pad_verifyTopFilePAD(st_PAD *pad);

#endif
