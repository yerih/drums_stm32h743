/*******************************************************************************
  * @file           : unitTest_stm32h743.h
  * @brief          : utilidades
  * @author			: Yerih Iturriago
  * @note			:
  *
  ******************************************************************************/

#ifndef UNIT_TEST_H_
#define UNIT_TEST_H_

#include "global.h"


/**********************************************************************
function: 	unitTest_SD_testing
input	:	mode: WRITE: crea el archivo y lo edita.
				  READ : lee el archivo.

output	:	si hubo algun error retorna 1. De lo contrario 0.
descr	:	realiza pruebas con la SD y muestra resultados con printf.
			Realiza: open, write, read, close.
***********************************************************************/
void unitTest_SD_testing(char* fileName);


/**********************************************************************
input	:	pad: pad correspondiente.
output	:	nothing
descr	:	realiza pruebas de lectura de un pad.
***********************************************************************/
void unitTest_readFilePAD(st_PAD* pad);


/**********************************************************************
input	:	pad:
output	:	noth
descr	:	lee el archivo sine.wav y lo coloca en el pad
***********************************************************************/
uint8_t unitTest_readSineFilePAD(st_PAD* pad);
#endif
