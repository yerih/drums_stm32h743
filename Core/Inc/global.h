/*******************************************************************************
  * @file           : global.h
  * @brief          : incluye todas las librerias y variables globales
  * @author			: Yerih Iturriago
  ******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdint.h>
#include <string.h>
#include "stm32h7xx_hal.h"
#include "stm32h7xx_hal_sai.h"
#include "main.h"
#include "usart.h"
#include "gpio.h"
#include "printf.h"
//#include "cmsis_os.h"
#include "fatfs.h"
#include "sdmmc.h"
#include "threads.h"
#include "fatfs.h"
#include "ff.h"
#include "dma.h"
#include "sai.h"
#include "i2s.h"
#include "sai_dma_OS.h"
#include "bsp_driver_sd.h"
#include "sd_diskio.h"
#include "ff_gen_drv.h"
#include "start.h"
#include "sd_os.h"
#include "utils.h"
#include "unitTest_stm32h743.h"
#include "audio_config.h"
#include "audio.h"
#include "pad_def.h"
#include "pad.h"


#ifndef GLOBAL_H_
#define GLOBAL_H_

#define FALSE 0
#define TRUE  1

/********************* LED configuracion ****************************/


#define LED_D3		  1
#define LED_TOGGLE_D3 HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_15)
#define LED_LOW_D3    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_15, GPIO_PIN_SET)
#define LED_HIGH_D3   HAL_GPIO_WritePin(GPIOB, GPIO_PIN_15, GPIO_PIN_RESET)

#define LED_SEQ_ERROR  -1
#define LED_SEQ_SUCCESS 0
#define LED_SEQ_THREAD	1
void led_sequence(int sequence);


/********************* variables app ****************************/

extern int g_showDebug;
extern int g_SDmntState;
extern st_AUDIO_BUFFER audioBuffer;


#endif



