Configuration	drums_stm32h743
STM32CubeMX 	6.0.1
Date	10/16/2020
MCU	STM32H743VITx



PERIPHERALS	MODES	FUNCTIONS	PINS
I2S1	Half-Duplex Master	I2S1_CK	PA5
I2S1	Half-Duplex Master	I2S1_SDO	PA7
I2S1	Half-Duplex Master	I2S1_WS	PA4
I2S1	Activated	I2S1_MCK	PC4
RCC	Crystal/Ceramic Resonator	RCC_OSC_IN	PH0-OSC_IN (PH0)
RCC	Crystal/Ceramic Resonator	RCC_OSC_OUT	PH1-OSC_OUT (PH1)
RCC	Crystal/Ceramic Resonator	RCC_OSC32_IN	PC14-OSC32_IN (OSC32_IN)
RCC	Crystal/Ceramic Resonator	RCC_OSC32_OUT	PC15-OSC32_OUT (OSC32_OUT)
SAI1:SAI A	Master with Master Clock Out	SAI1_SD_A	PE6
SAI1:SAI A	Master with Master Clock Out	SAI1_SCK_A	PE5
SAI1:SAI A	Master with Master Clock Out	SAI1_FS_A	PE4
SAI1:SAI A	Master with Master Clock Out	SAI1_MCLK_A	PE2
SAI1:SAI A	Basic	SAI1_VP_$IpInstance_SAIA_SAI_BASIC	VP_SAI1_VP_$IpInstance_SAIA_SAI_BASIC
SDMMC1	SD 4 bits Wide bus with auto dir voltage converter	SDMMC1_CK	PC12
SDMMC1	SD 4 bits Wide bus with auto dir voltage converter	SDMMC1_CKIN	PB8
SDMMC1	SD 4 bits Wide bus with auto dir voltage converter	SDMMC1_CMD	PD2
SDMMC1	SD 4 bits Wide bus with auto dir voltage converter	SDMMC1_D0	PC8
SDMMC1	SD 4 bits Wide bus with auto dir voltage converter	SDMMC1_D1	PC9
SDMMC1	SD 4 bits Wide bus with auto dir voltage converter	SDMMC1_D2	PC10
SDMMC1	SD 4 bits Wide bus with auto dir voltage converter	SDMMC1_D3	PC11
SYS	SysTick	SYS_VS_Systick	VP_SYS_VS_Systick
UART4	Asynchronous	UART4_RX	PA1
UART4	Asynchronous	UART4_TX	PA0



Pin Nb	PINs	FUNCTIONs	LABELs
1	PE2	SAI1_MCLK_A	
3	PE4	SAI1_FS_A	
4	PE5	SAI1_SCK_A	
5	PE6	SAI1_SD_A	
8	PC14-OSC32_IN (OSC32_IN)	RCC_OSC32_IN	
9	PC15-OSC32_OUT (OSC32_OUT)	RCC_OSC32_OUT	
12	PH0-OSC_IN (PH0)	RCC_OSC_IN	
13	PH1-OSC_OUT (PH1)	RCC_OSC_OUT	
22	PA0	UART4_TX	
23	PA1	UART4_RX	
28	PA4	I2S1_WS	
29	PA5	I2S1_CK	
31	PA7	I2S1_SDO	
32	PC4	I2S1_MCK	
54	PB15	GPIO_Output	
65	PC8	SDMMC1_D0	
66	PC9	SDMMC1_D1	
78	PC10	SDMMC1_D2	
79	PC11	SDMMC1_D3	
80	PC12	SDMMC1_CK	
83	PD2	SDMMC1_CMD	
95	PB8	SDMMC1_CKIN	
PERIPHERALS	MODES	FUNCTIONS	PINS
I2S1	Half-Duplex Master	I2S1_CK	PA5
I2S1	Half-Duplex Master	I2S1_SDO	PA7
I2S1	Half-Duplex Master	I2S1_WS	PA4
I2S1	Activated	I2S1_MCK	PC4
RCC	Crystal/Ceramic Resonator	RCC_OSC_IN	PH0-OSC_IN (PH0)
RCC	Crystal/Ceramic Resonator	RCC_OSC_OUT	PH1-OSC_OUT (PH1)
RCC	Crystal/Ceramic Resonator	RCC_OSC32_IN	PC14-OSC32_IN (OSC32_IN)
RCC	Crystal/Ceramic Resonator	RCC_OSC32_OUT	PC15-OSC32_OUT (OSC32_OUT)
SAI1:SAI A	Master with Master Clock Out	SAI1_SD_A	PE6
SAI1:SAI A	Master with Master Clock Out	SAI1_SCK_A	PE5
SAI1:SAI A	Master with Master Clock Out	SAI1_FS_A	PE4
SAI1:SAI A	Master with Master Clock Out	SAI1_MCLK_A	PE2
SAI1:SAI A	Basic	SAI1_VP_$IpInstance_SAIA_SAI_BASIC	VP_SAI1_VP_$IpInstance_SAIA_SAI_BASIC
SDMMC1	SD 4 bits Wide bus with auto dir voltage converter	SDMMC1_CK	PC12
SDMMC1	SD 4 bits Wide bus with auto dir voltage converter	SDMMC1_CKIN	PB8
SDMMC1	SD 4 bits Wide bus with auto dir voltage converter	SDMMC1_CMD	PD2
SDMMC1	SD 4 bits Wide bus with auto dir voltage converter	SDMMC1_D0	PC8
SDMMC1	SD 4 bits Wide bus with auto dir voltage converter	SDMMC1_D1	PC9
SDMMC1	SD 4 bits Wide bus with auto dir voltage converter	SDMMC1_D2	PC10
SDMMC1	SD 4 bits Wide bus with auto dir voltage converter	SDMMC1_D3	PC11
SYS	SysTick	SYS_VS_Systick	VP_SYS_VS_Systick
UART4	Asynchronous	UART4_RX	PA1
UART4	Asynchronous	UART4_TX	PA0



Pin Nb	PINs	FUNCTIONs	LABELs
1	PE2	SAI1_MCLK_A	
3	PE4	SAI1_FS_A	
4	PE5	SAI1_SCK_A	
5	PE6	SAI1_SD_A	
8	PC14-OSC32_IN (OSC32_IN)	RCC_OSC32_IN	
9	PC15-OSC32_OUT (OSC32_OUT)	RCC_OSC32_OUT	
12	PH0-OSC_IN (PH0)	RCC_OSC_IN	
13	PH1-OSC_OUT (PH1)	RCC_OSC_OUT	
22	PA0	UART4_TX	
23	PA1	UART4_RX	
28	PA4	I2S1_WS	
29	PA5	I2S1_CK	
31	PA7	I2S1_SDO	
32	PC4	I2S1_MCK	
54	PB15	GPIO_Output	
65	PC8	SDMMC1_D0	
66	PC9	SDMMC1_D1	
78	PC10	SDMMC1_D2	
79	PC11	SDMMC1_D3	
80	PC12	SDMMC1_CK	
83	PD2	SDMMC1_CMD	
95	PB8	SDMMC1_CKIN	



SOFTWARE PROJECT

Project Settings : 
Project Name : drums_stm32h743
Project Folder : D:\DRUMS project\STM32F4\STM32CubeIDE\STM32CubeIDE\drums_stm32h743
Toolchain / IDE : STM32CubeIDE
Firmware Package Name and Version : STM32Cube FW_H7 V1.8.0


Code Generation Settings : 
STM32Cube MCU packages and embedded software packs : Copy only the necessary library files
Generate peripheral initialization as a pair of '.c/.h' files per peripheral : Yes
Backup previously generated files when re-generating : No
Delete previously generated files when not re-generated : Yes
Set all free pins as analog (to optimize the power consumption) : No


Toolchains Settings : 
Compiler Optimizations : 





